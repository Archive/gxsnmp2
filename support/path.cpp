// -*- Mode: C++ -*-
// $Id$
// path.cpp -- Path resolver implementation.
//
#ifdef HAVE_CONFIG_H
#include <config.h>
#endif
#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <limits.h>

#include "path.h"

Path::Path()
{
  m_path = 0;
}

Path::Path(const char *file, PathType type)
{
  m_path = 0;
  resolve (file, type);
}

// Resolve the path for a file of the PathType type.
// FIXME: Improve this to do better resolution.
const char *
Path::resolve (const char *file, PathType type)
{
  char       path[PATH_MAX];
  const char *sub_path;

  free_path ();
  switch (type)
    {
    case PATH_DATA:
      sub_path = "data";
      break;
    case PATH_ICON:
      sub_path = "pixmaps";
      break;
    default:
      sub_path = PATH_CUR;
      break;
    }
  snprintf (path, sizeof (path), "%s"PATH_SEP"%s"PATH_SEP"%s", PATH_CUR, 
	sub_path, file);
    for (char *bufp = path; *bufp; ++bufp)
    {
      if ( (bufp[0] == *PATH_SEP) && (bufp[1] == *PATH_SEP) )
        {
          strcpy (bufp, bufp+1);
        }
    }
  m_path = new char[strlen(path)+1];
  strcpy (m_path, path);
  return (m_path);
}

void
Path::free_path (void)
{
  if (m_path)
    {
      delete [] m_path;
      m_path = 0;
    }
}

/* EOF */
