/* -*- Mode: C; tab-width: 8; indent-tabs-mode: nil; c-basic-offset: 4 -*-
 * $Id$
 * gxsnmp_database_client.c -- gxsnmp database client handling code.
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc.,  59 Temple Place - Suite 330, Cambridge, MA 02139, USA.
 *
 *  gxsnmp_database_client.c
 *
 */
#ifdef HAVE_CONFIG_H
#include <config.h>
#endif

/**
 * gxsnmp_database_read_header:
 *
 * This will read the header on the incomming data stream to determine how
 * much it has to read.
 *
 * Return value: number of bytes read or -1 on error.
 **/
gint
gxsnmp_database_read_header (gint sock, char *buffer, gint mode)
{
  gint   nbytes;
  gint   retry;
  
  retry = 0;
 read_again:
  nbytes = read (sock, buffer, GXSNMP_DB_HEADER);
  if (nbytes == -1)
    {
      if (errno == EAGAIN)
	{
	  if (retry >= 3)
	    return -1;
	  retry++;
	  sleep (1);
	  goto read_again:
	}
    }
  else
    {
      g_warning (g_strerror (errno));
      return -1;
    }
  return nbytes;
}

gint
gxsnmp_database_write (gint sock, gchar *request)
{
  gint nbytes;
  nbytes = write (sock, data, datalen + 1);
  return nbytes;
}

