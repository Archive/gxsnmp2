/* -*- Mode: C; tab-width: 8; indent-tabs-mode: nil; c-basic-offset: 4 -*-
 * $Id$
 * GXSNMP - An snmp managment application
 * Copyright (C) 1998 Gregory McLean
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc.,  59 Temple Place - Suite 330, Cambridge, MA 02139, USA.
 *
 * Some generic date conversion type routines.
 */

#include "config.h"
#include <stdio.h>
#include <sys/time.h>
#include <glib.h>
#include "g_date.h"

/* 
 * take the timeticks and convert it to a _human_ readable string
 */
gchar *
timetick_string (gulong timeticks, gchar *buf)
{
  int     seconds,
          minutes,
          hours,
          days;

  timeticks /= 100;
  days = timeticks / (60 * 60 * 24);
  timeticks %= (60 * 60 * 24);

  hours = timeticks / (60 * 60);
  timeticks %= (60 * 60);

  minutes = timeticks / 60;
  seconds = timeticks % 60;

  if (days == 0)
    {
      sprintf (buf, "%d:%02d:%02d", hours, minutes, seconds);
    } else 
      if (days == 1)
	{
	  sprintf (buf, "%d day, %d:%02d:%02d", days, hours, 
		   minutes, seconds);
	} else
	  {
	    sprintf (buf, "%d days, %d:%02d:%02d", days, hours,
		      minutes, seconds);
	  }
  return buf;
}

/* EOF */

