// -*- Mode: C++; tab-width: 4; indent-tabs-mode: nil; c-basic-offset: 4 -*- 
// $Id$
// path.h  -- A simple path resolver.
//
//

#ifndef _PATH_H__
#define _PATH_H__

typedef enum
{
    PATH_DATA,                 // Where we store data.
    PATH_ICON                  // Icon locations
} PathType;

// Unix uses the following path seperators, if you port it somewhere
// else that has different separtors here is where you would add code
// for that.
#define PATH_SEP                 "/"
#define PATH_CUR                 "."

class Path {
public:
    Path ();
    Path (const char *file, PathType type = PATH_DATA);  // The default
    ~Path();
    
    // Use this where you might need a string
    operator const char *() {
        return (m_path);
    }
    
    const char *resolve (const char *file, PathType type = PATH_DATA);
    void       free_path (void);
protected:
    char       *m_path;
};

#endif
// EOF 
