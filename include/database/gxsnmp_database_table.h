/* -*- Mode: C; tab-width: 4; indent-tabs-mode: nil; c-basic-offset: 4 -*-
 *  $Id$
 *  GXSNMP - An snmp management application
 *
 *  Copyright 2000 Gregory McLean and others.
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc.,  59 Temple Place - Suite 330, Cambridge, MA 02139, USA.
 *
 */
#ifndef _GXSNMP_DB_TABLES_H__
#define _GXSNMP_DB_TABLES_H__

/**
 * gxsnmp_db_table_descriptor
 *
 **/
typedef struct _gxsnmp_db_table {
    gchar         *name;
    guint         size;
    GList         *fields;
} gxsnmp_db_table;

/**
 * GXSNMP_DB_FIELD_TYPES
 *
 **/
typedef enum {
  TD_GUINT = 1,
  TD_GINT,
  TD_GSTR,
  TD_PGUINT,
  TD_PGINT,
  TD_PGSTR,
  TD_PGL,
  TD_GDOUBLE,
  TD_PGDOUBLE
} GxSNMP_DB_FIELD_TYPE;

/**
 * gxsnmp_db_field_descriptor
 *
 **/
typedef struct _gxsnmp_db_field {
    gchar                 *name;
    GxSNMP_DB_FIELD_TYPE  type;
    guint                 offset;
} gxsnmp_db_field;

/****************************************************************************
 * Public functions.
 **/
gint                gxsnmp_db_table_size            (void);
gint                gxsnmp_db_table_offset          (void);
gxsnmp_db_table     *gxsnmp_db_table_new            (void);
gxsnmp_db_table     *gxsnmp_db_table_find           (const gchar      *name);

gxsnmp_db_field     *gxsnmp_db_field_new            (void);
gint                gxsnmp_db_field_offset          (gxsnmp_db_table  *table,
                                                     const gchar      *name);
gint                gxsnmp_db_field_type            (gxsnmp_db_table  *table,
                                                     const gchar      *name);


#endif



