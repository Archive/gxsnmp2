/* -*- Mode: C -*-
 * $Id$
 * gxsnmp_database_api.h -- Common api definitions and functions between
 *                          the server and client.
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc.,  59 Temple Place - Suite 330, Cambridge, MA 02139, USA.
 *
 */
#ifndef _GXSNMP_DATABASE_API_H__
#define _GXSNMP_DATABASE_API_H__

typedef enum {
  GXSNMP_DB_AUTH = 1,
  GXSNMP_DB_NACK,
  GXSNMP_DB_NOTIFY,
  GXSNMP_DB_ADD,
  GXSNMP_DB_DELETE,
  GXSNMP_DB_UPDATE,
  GXSNMP_DB_READ,
  GXSNMP_DB_TABLE_LOAD,
  GXSNMP_DB_RESULT
} GxSNMP_DB_CMD;

#endif
