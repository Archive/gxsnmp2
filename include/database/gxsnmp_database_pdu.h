
#ifndef _GXSNMP_DATABASE_PDU_H__
#define _GXSNMP_DATABASE_PDU_H__

/*
 * PDU Types
 */
typedef enum {
  PDUMEMTYPE_NULL = 1,
  PDUMEMTYPE_INT,
  PDUMEMTYPE_STR,
  PDUMEMTYPE_DOUBLE
} GxSNMP_PDU_Types;


/***************************************************************************/
void            gxsnmp_db_decode_output          (gint           *offset,
						  unsigned char  *packet,
						  gint           rewind,
						  gint           len);
gint            gxsnmp_db_get_member             (gint           *n,
						  gpointer       packet,
						  gpointer       buffer);
void            gxsnmp_db_add_guint              (gint           *n,
						  gpointer       packet,
						  guint          data);
void            gxsnmp_db_add_gdouble            (gint           *n,
						  gpointer       packet,
						  gdouble        data);
void            gxsnmp_db_add_string             (gint           *n,
						  gpointer       packet,
						  gpointer       str);
gint            gxsnmp_db_set_pdu_header         (gpointer       packet,
						  gint           type,
						  gchar          *table_name);
#endif
