/* -*- Mode: C -*-
 * $Id$
 * gxsnmp_database_client.h -- The client interface to the database backend.
 * This is the only interface between clients and the database server.
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc.,  59 Temple Place - Suite 330, Cambridge, MA 02139, USA.
 */
#ifndef _GXSNMP_DATABASE_CLIENT_H__
#define _GXSNMP_DATABASE_CLIENT_H__

#include <glib.h>

/* This defines how big a header is */
#define GXSNMP_DB_HEADER (sizeof (gint) * 2)
gint            gxsnmp_database_read_header          (gint        sock,
						      gchar       *buffer,
						      gint        mode);
gint            gxsnmp_database_connect              (gchar       *server,
						      gint        port);
void            gxsnmp_database_disconnect           (gint        sock);
gint            gxsnmp_database_write                (gint        sock,
						      gchar       *request);
gchar           *gxsnmp_database_read                (gint        sock);









