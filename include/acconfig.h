
/* auto* support */
#undef PACKAGE
#undef VERSION

/* For internationalization */
#undef ENABLE_NLS
#undef HAVE_STPCPY
#undef HAVE_GETTEXT
#undef HAVE_CATGETS
#undef HAVE_LC_MESSAGES

/* BONOBO goo */
#undef BONOBO_POST_0_15
#undef USING_OAF

/* Network Support */
#undef HAVE_INET
