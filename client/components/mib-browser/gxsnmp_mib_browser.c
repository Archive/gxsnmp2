/* -*- Mode: C -*-
 * $Id$
 * GXSNMP - An snmp managment application
 * Copyright (C) 1998 - 2001 Gregory McLean, Jochen Friedrich
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc.,  59 Temple Place - Suite 330, Cambridge, MA 02139, USA.
 *
 * The mib browser.
 */
#ifdef HAVE_CONFIG_H
#include <config.h>
#endif

#include <gnome.h>

#include <smi.h>
#include "g_snmp.h"

#include "gxsnmp_mib_browser.h"
#include "gxsnmp_util.h"

#include "debug.h"

/****************************************************************************
 * Static data
 ***************************************************************************/
#define BROWSER_LABELS 6
static gchar *browser_labels[] = {
  N_("Object Type"), N_("Module"), N_("OID"), N_("Status"), N_("Label"), 
  N_("Value")
};

static const value_string smi_basetype[] = {
  { SMI_BASETYPE_INTEGER32, 		N_("Integer32")},
  { SMI_BASETYPE_OCTETSTRING, 		N_("String")},
  { SMI_BASETYPE_OBJECTIDENTIFIER,	N_("Object ID")},
  { SMI_BASETYPE_UNSIGNED32,		N_("Unsigned32")},
  { SMI_BASETYPE_INTEGER64, 		N_("Integer64")},
  { SMI_BASETYPE_UNSIGNED64,		N_("Unsigned64")},
  { SMI_BASETYPE_FLOAT32,		N_("Float32")},
  { SMI_BASETYPE_FLOAT64,		N_("Float64")},
  { SMI_BASETYPE_FLOAT128,		N_("Float128")},
  { SMI_BASETYPE_ENUM,			N_("Enum")},
  { SMI_BASETYPE_BITS,			N_("Bits")},
  { SMI_BASETYPE_UNKNOWN,		N_("Unknown")},
  { 0, NULL}};

static const value_string smi_status[] = {
  { SMI_STATUS_CURRENT, 		N_("Current")},
  { SMI_STATUS_DEPRECATED, 		N_("Deprecated")},
  { SMI_STATUS_MANDATORY, 		N_("Mandatory")},
  { SMI_STATUS_OPTIONAL, 		N_("Optional")},
  { SMI_STATUS_OBSOLETE, 		N_("Obsolete")},
  { SMI_STATUS_UNKNOWN, 		N_("Unknown")},
  { 0, NULL}};

static const value_string smi_node_pixmap[] = {
  { SMI_NODEKIND_NODE,			"gxsnmp/node_node.xpm"},
  { SMI_NODEKIND_SCALAR,		"gxsnmp/node_scalar.xpm"},
  { SMI_NODEKIND_TABLE,			"gxsnmp/node_table.xpm"},
  { SMI_NODEKIND_ROW,			"gxsnmp/node_row.xpm"},
  { SMI_NODEKIND_COLUMN,		"gxsnmp/node_column.xpm"},
  { SMI_NODEKIND_NOTIFICATION,		"gxsnmp/node_notification.xpm"},
  { SMI_NODEKIND_GROUP,			"gxsnmp/node_group.xpm"},
  { SMI_NODEKIND_COMPLIANCE,		"gxsnmp/node_compliance.xpm"},
  { SMI_NODEKIND_UNKNOWN,		"gxsnmp/node_unknown.xpm"},
  { 0, NULL}};

static const value_string snmp_error[] = {
  { SNMP_TOOBIG,			"too big"},
  { SNMP_NOSUCHNAME,			"no such name"},
  { SNMP_BADVALUE,			"bad value"},
  { SNMP_READONLY,			"read only"},
  { SNMP_GENERROR,			"generic error"},
  { SNMP_NOACCESS,			"no access"},
  { SNMP_WRONGTYPE,			"wrong type"},
  { SNMP_WRONGLENGTH,			"wrong length"},
  { SNMP_WRONGENCODING,			"wrong encoding"},
  { SNMP_WRONGVALUE,			"wrong value"},
  { SNMP_NOCREATION,			"no creation"},
  { SNMP_INCONSISTENTVALUE,		"inconsisten value"},
  { SNMP_RESOURCEUNAVAILABLE,		"resource unavailable"},
  { SNMP_COMMITFAILED,			"commit failed"},
  { SNMP_UNDOFAILED,			"undo failed"},
  { SNMP_AUTHORIZATIONERROR,		"authorization error"},
  { SNMP_NOTWRITABLE,			"not writable"},
  { SNMP_INCONSISTENTNAME,		"inconsistent name"},
  { 0, NULL}};

enum
{
  TARGET_OID,
  TARGET_STRING
};

static const GtkTargetEntry drag_targets[] = {
  { "application/x-gxsnmp-oid", 0, TARGET_OID },
  { "text/plain", 0, TARGET_STRING }
};

static host_snmp shost;

/****************************************************************************
 * Forward declarations and callback functions  
 ***************************************************************************/
static void      browser_class_init       (GxSNMPMibBrowserClass     *klass);
static void      browser_init             (GxSNMPMibBrowser          *browser);
static void	 cb_expand		  (GtkCTree		     *ctree,
					   GtkCTreeNode		     *node,
					   gpointer		      data);
static void      cb_select_row            (GtkCTree                  *ctree,
					   gint			      row,
					   gint			      col,
					   GdkEvent		     *e,
                                           gpointer                   data);
static void	 mib_tree_drag_begin	  (GtkWidget		     *widget,
					   GdkDragContext	     *context,
					   gpointer		      data);
static void	 mib_tree_data_get	  (GtkWidget		     *widget,
					   GdkDragContext	     *context,
					   GtkSelectionData	     *sel_data,
					   guint		      info,
					   guint		      dtime,
					   gpointer		      data);
static void	 walk_button_cb		  (GtkWidget		     *widget,
					   gpointer		      data);
static void	 table_button_cb	  (GtkWidget		     *widget,
					   gpointer		      data);
static void	 get_button_cb		  (GtkWidget		     *widget,
					   gpointer		      data);
static void	 put_button_cb		  (GtkWidget		     *widget,
					   gpointer		      data);
gboolean	 update_mib_value	  (host_snmp		     *host,
					   void			     *magic,
					   SNMP_PDU		     *spdu,
					   GSList		     *objs);
void		 update_mib_timeout	  (host_snmp                 *host,
					   void                      *magic);
gboolean	 walk_mib_value		  (host_snmp		     *host,
					   void			     *magic,
					   SNMP_PDU		     *spdu,
					   GSList		     *objs);
void		 walk_mib_timeout	  (host_snmp                 *host,
					   void                      *magic);
/****************************************************************************
 * Class registration
 ***************************************************************************/
/**
 * gxsnmp_mib_browser_get_type:
 *
 * Registers class with GTK and returns handle ("type") of class. The handle
 * is cached in a static class variable for repeated calls of this function.
 *
 * Return value: unique type
 **/
GtkType
gxsnmp_mib_browser_get_type ()
{
  static GtkType list_type = 0;
  D_FUNC_START;
  if (!list_type)
    {
      GtkTypeInfo list_info = 
      {
	"GxSNMPMibBrowser",
	sizeof (GxSNMPMibBrowser),
	sizeof (GxSNMPMibBrowserClass),
	(GtkClassInitFunc) browser_class_init,
	(GtkObjectInitFunc) browser_init,
	/* reserved 1 */ NULL,
	/* reserved 2 */ NULL,
	(GtkClassInitFunc) NULL,
      };
      list_type =gtk_type_unique(gtk_vbox_get_type(), &list_info);
    }
  D_FUNC_END;
  return list_type;
}
/****************************************************************************
 * Static Helper functions
 ***************************************************************************/
/**
 * getoid:
 * @node: SMI node whose OID should be formatted
 * @buffer: result buffer
 * @buflen: length of buffer
 *
 * Formats an OID of a node into a preallocated buffer.
 *
 **/
static void
getoid (SmiNode *node, char *buffer, int buflen)
{
  int i;
  D_FUNC_START;
  snprintf(buffer, buflen, "%d", node->oid[0]);
  for (i=1; i < node->oidlen; i++)
    snprintf(buffer + strlen (buffer), buflen - strlen (buffer), ".%d", 
             node->oid[i]);
  D_FUNC_END;
}
/**
 * add_oid:
 * @objs: object structure
 * @oid: OID in SMI notation
 * @oidlen: length of OID
 * @iid: Instance in SMI notation
 * @iidlen: length of Instance
 * @type: type of object
 * @value: value of object
 *
 * setup host_snmp structure from object data.
 *
 **/
void
add_oid(GSList **objs, gint *oid, gint oidlen, gint *iid, gint iidlen, 
        guchar type, gpointer value)
{
  gulong *mib;
  guint   miblen;
  D_FUNC_START;
  mib = g_malloc((oidlen + iidlen) * sizeof(gulong));
  for (miblen = 0; miblen < oidlen; miblen++)
    mib[miblen] = oid[miblen];
  for (miblen = 0; miblen < iidlen; miblen++)
    mib[oidlen + miblen] = iid[miblen];
  miblen = oidlen + iidlen;
  g_pdu_add_oid(objs, mib, miblen, type, value);
  D_FUNC_END;
}
/****************************************************************************
 * Class methods
 ***************************************************************************/
/**
 * browser_class_init:
 * @klass: self 
 *
 * Constructor of this class.
 *
 **/
static void
browser_class_init (GxSNMPMibBrowserClass *klass)
{
  D_FUNC_START;
  D_FUNC_END;
}
/****************************************************************************
 * Object methods
 ***************************************************************************/
/**
 * create_tree_item:
 * @browser: self
 * @ctree: ctree object
 * @parent: parent tree item
 * @node: SMI node to create tree item for
 *
 * Creates the subtree of the given node and attach it to the parent.
 *
 **/
static void 
create_tree_item(GtkWidget *browser, GtkCTree *ctree, GtkCTreeNode *parent, 
		 SmiNode *node) 
{
  char buffer[255];
  char *buf[1];
  char *pbuffer;
  GtkCTreeNode *item;
  GtkCTreeNode *dummy;
  //GnomePixmap *pixmap;
  GdkPixmap *pixmap;
  GdkBitmap *mask;
  SmiNode *child;
  D_FUNC_START;
  if (node->name)
    snprintf(buffer, 255, "%s (%d)", node->name, node->oid[node->oidlen-1]);
  else
    snprintf(buffer, 255, "(%d)", node->oid[node->oidlen-1]);
  pbuffer = match_strval (node->nodekind, smi_node_pixmap);
  if (!pbuffer)
    {
      g_warning("Unknown nodekind found: %d", node->nodekind);
      pbuffer = "gxsnmp/node_unknown.xpm";
    }
  //pixmap = GNOME_PIXMAP(gnome_stock_pixmap_widget(browser, pbuffer));
  pixmap = gdk_pixmap_create_from_xpm (browser->window, &mask, NULL, pbuffer);
  buf[0] = buffer;
  item = gtk_ctree_insert_node (ctree, parent, NULL, buf, 1,
				pixmap, mask, 
		 		pixmap, mask,
				FALSE, FALSE);
  gtk_ctree_node_set_row_data(ctree, item, node);
  if ((child = smiGetFirstChildNode(node)))
    {
      strcpy(buffer,"!dummy!");
      buf[0] = buffer;
      dummy = gtk_ctree_insert_node(ctree, item, NULL, buf, 1,
                                NULL, NULL, NULL, NULL, TRUE, FALSE);
    }
  D_FUNC_END;
}
/**
 * browser_init:
 * @browser: self 
 *
 * Constructor of this object.
 *
 * Constructs GUI objects.
 *
 **/
static void
browser_init (GxSNMPMibBrowser *browser)
{
  GtkWidget    *hbox;
  GtkWidget    *vbox;
  GtkWidget    *table;
  GtkWidget    *frame;
  GtkWidget    *label;
  GtkWidget    *hscroll;
  GtkWidget    *vscroll;
  GtkWidget    *bar;
  GtkWidget    *button_box;
  GtkWidget    *scrolled_window;
  SmiNode      *node;
  int           i;
  GdkColor      red;
  D_FUNC_START;
  hbox = gtk_hbox_new (FALSE, 2);
  gtk_box_pack_start (GTK_BOX (GTK_VBOX(browser)), 
                      hbox, TRUE, TRUE, 0);
/* Left side shows MIB tree */
  vbox = gtk_vbox_new (FALSE, 2);
  gtk_box_pack_start (GTK_BOX (hbox), vbox, TRUE, TRUE, 0);
  scrolled_window = gtk_scrolled_window_new (NULL, NULL);
  gtk_scrolled_window_set_policy (GTK_SCROLLED_WINDOW (scrolled_window),
				  GTK_POLICY_AUTOMATIC, GTK_POLICY_AUTOMATIC);
  gtk_box_pack_start (GTK_BOX (vbox), scrolled_window, TRUE, TRUE, 0);
  gtk_widget_set_usize (GTK_WIDGET (scrolled_window), 250, 250);
  browser->root = gtk_ctree_new (1, 0);
  gtk_clist_set_selection_mode (GTK_CLIST (browser->root), GTK_SELECTION_SINGLE);
  gtk_clist_set_column_auto_resize (GTK_CLIST (browser->root), 0, TRUE);
  gtk_ctree_set_show_stub (GTK_CTREE (browser->root), FALSE);
  gtk_ctree_set_indent (GTK_CTREE (browser->root), 10);
  gtk_widget_set_sensitive (GTK_WIDGET (browser->root), FALSE);
  gtk_drag_source_set (GTK_WIDGET (browser->root), GDK_BUTTON1_MASK,
                       drag_targets, ELEMENTS (drag_targets), GDK_ACTION_COPY);
  gtk_signal_connect (GTK_OBJECT (browser->root), "drag_begin",
                     (GtkSignalFunc) mib_tree_drag_begin, browser);
  gtk_signal_connect (GTK_OBJECT (browser->root), "drag_data_get",
                     (GtkSignalFunc) mib_tree_data_get, browser);
  gtk_scrolled_window_add_with_viewport 
                        (GTK_SCROLLED_WINDOW (scrolled_window), browser->root);
/* Right side shows entry fields */
  vbox = gtk_vbox_new (FALSE, 2);
  gtk_box_pack_start (GTK_BOX (hbox), vbox, FALSE, FALSE, 2);
  frame = gtk_frame_new (NULL);
  gtk_box_pack_start (GTK_BOX (vbox), frame, FALSE, FALSE, 0);
  table = gtk_table_new ( 2, 7, FALSE);
  gtk_container_add (GTK_CONTAINER (frame), table);
/* Create labels */
  for (i=0; i<BROWSER_LABELS; i++)
    { 
      label = gtk_label_new (gettext(browser_labels[i]));
      gtk_misc_set_alignment (GTK_MISC (label), 0.0, 0.5);
      gtk_table_attach (GTK_TABLE (table), label, 0, 1, i, i+1,
		        GTK_SHRINK | GTK_FILL, GTK_SHRINK, 2, 2);
    }
/* Create SNMP updatable fields */
  browser->mib_type = gtk_entry_new ();
  gtk_entry_set_editable (GTK_ENTRY (browser->mib_type), FALSE);
  gtk_table_attach (GTK_TABLE (table), browser->mib_type,
		    1, 2, 0, 1,
		    GTK_EXPAND | GTK_SHRINK | GTK_FILL, GTK_SHRINK, 2, 2);
  browser->mib_module = gtk_entry_new ();
  gtk_entry_set_editable (GTK_ENTRY (browser->mib_module), FALSE);
  gtk_table_attach (GTK_TABLE (table), browser->mib_module,
		    1, 2, 1, 2,
		    GTK_EXPAND | GTK_SHRINK | GTK_FILL, GTK_SHRINK, 2, 2);
  browser->mib_oid = gtk_entry_new ();
  gtk_entry_set_editable (GTK_ENTRY (browser->mib_oid), FALSE);
  gtk_table_attach (GTK_TABLE (table), browser->mib_oid, 
		    1, 2, 2, 3,
		    GTK_EXPAND | GTK_SHRINK | GTK_FILL, GTK_SHRINK, 2, 2);
  browser->mib_status = gtk_entry_new ();
  gtk_entry_set_editable (GTK_ENTRY (browser->mib_status), FALSE);
  gtk_table_attach (GTK_TABLE (table), browser->mib_status, 
		    1, 2, 3, 4,
		    GTK_EXPAND | GTK_SHRINK | GTK_FILL, GTK_SHRINK, 2, 2);
  browser->mib_label = gtk_entry_new ();
  gtk_entry_set_editable (GTK_ENTRY (browser->mib_label), FALSE);
  gtk_table_attach (GTK_TABLE (table), browser->mib_label,
		    1, 2, 4, 5, 
		    GTK_EXPAND | GTK_SHRINK | GTK_FILL, GTK_SHRINK, 2, 2);
  browser->mib_value = gtk_entry_new ();
  gtk_table_attach (GTK_TABLE (table), browser->mib_value,
		    1, 2, 5, 6,
		    GTK_SHRINK | GTK_FILL, GTK_SHRINK, 2, 2);
/* Notebook */
  browser->notebook = gtk_notebook_new();
  gtk_box_pack_start (GTK_BOX (vbox), browser->notebook, FALSE, FALSE, 0);
/* Text entry field for displaying description */
  label = gtk_label_new (_("Description"));
  gtk_misc_set_alignment (GTK_MISC (label), 0.0, 0.1);
  table = gtk_table_new (2, 2, FALSE);
  gtk_table_set_row_spacing (GTK_TABLE (table), 0, 2);
  gtk_table_set_col_spacing (GTK_TABLE (table), 0, 2);
  gtk_notebook_append_page (GTK_NOTEBOOK (browser->notebook), table, label);
  browser->dpage = gtk_text_view_new ();
  gtk_text_view_set_editable (GTK_TEXT_VIEW (browser->dpage), FALSE);
  gtk_widget_set_usize (GTK_WIDGET (browser->dpage), 350, 200);
  gtk_text_view_set_wrap_mode (GTK_TEXT_VIEW(browser->dpage), GTK_WRAP_NONE);
  gtk_table_attach_defaults (GTK_TABLE (table), browser->dpage, 0, 1, 0, 1);
//  hscroll = gtk_hscrollbar_new (GTK_TEXT_VIEW (browser->dpage)->hadj);
//  gtk_table_attach (GTK_TABLE (table), hscroll, 0, 1, 1, 2,
//		    GTK_EXPAND | GTK_FILL, GTK_FILL, 0, 0);
//  vscroll = gtk_vscrollbar_new (GTK_TEXT (browser->dpage)->vadj);
//  gtk_table_attach (GTK_TABLE (table), vscroll, 1, 2, 0, 1,
//		    GTK_FILL, GTK_EXPAND | GTK_FILL, 0, 0);
/* Text entry field for displaying found objects */
  label = gtk_label_new (_("Walk Result"));
  gtk_misc_set_alignment (GTK_MISC (label), 0.0, 0.1);
  table = gtk_table_new (2, 2, FALSE);
  gtk_table_set_row_spacing (GTK_TABLE (table), 0, 2);
  gtk_table_set_col_spacing (GTK_TABLE (table), 0, 2);
  gtk_notebook_append_page (GTK_NOTEBOOK (browser->notebook), table, label);
  browser->wpage = gtk_text_view_new ();
  gtk_text_view_set_editable (GTK_TEXT_VIEW (browser->wpage), FALSE);
  gtk_widget_set_usize (GTK_WIDGET (browser->wpage), 350, 200);
  gtk_text_view_set_wrap_mode (GTK_TEXT_VIEW(browser->wpage), GTK_WRAP_NONE);
  gtk_table_attach_defaults (GTK_TABLE (table), browser->wpage, 0, 1, 0, 1);
//  hscroll = gtk_hscrollbar_new (GTK_TEXT (browser->wpage)->hadj);
//  gtk_table_attach (GTK_TABLE (table), hscroll, 0, 1, 1, 2,
//		    GTK_EXPAND | GTK_FILL, GTK_FILL, 0, 0);
//  vscroll = gtk_vscrollbar_new (GTK_TEXT (browser->wpage)->vadj);
//  gtk_table_attach (GTK_TABLE (table), vscroll, 1, 2, 0, 1,
//		    GTK_FILL, GTK_EXPAND | GTK_FILL, 0, 0);
/* Text entry field for displaying found objects */
  label = gtk_label_new (_("Table Result"));
  gtk_misc_set_alignment (GTK_MISC (label), 0.0, 0.1);
  table = gtk_table_new (2, 2, FALSE);
  gtk_table_set_row_spacing (GTK_TABLE (table), 0, 2);
  gtk_table_set_col_spacing (GTK_TABLE (table), 0, 2);
  gtk_notebook_append_page (GTK_NOTEBOOK (browser->notebook), table, label);
  browser->tpage = gtk_text_view_new ();
  gtk_text_view_set_editable (GTK_TEXT_VIEW (browser->tpage), FALSE);
  gtk_widget_set_usize (GTK_WIDGET (browser->tpage), 350, 200);
  gtk_text_view_set_wrap_mode (GTK_TEXT_VIEW(browser->tpage), GTK_WRAP_NONE);
  gtk_table_attach_defaults (GTK_TABLE (table), browser->tpage, 0, 1, 0, 1);
//  hscroll = gtk_hscrollbar_new (GTK_TEXT (browser->tpage)->hadj);
//  gtk_table_attach (GTK_TABLE (table), hscroll, 0, 1, 1, 2,
//		    GTK_EXPAND | GTK_FILL, GTK_FILL, 0, 0);
//  vscroll = gtk_vscrollbar_new (GTK_TEXT (browser->tpage)->vadj);
//  gtk_table_attach (GTK_TABLE (table), vscroll, 1, 2, 0, 1,
//		    GTK_FILL, GTK_EXPAND | GTK_FILL, 0, 0);
/* Buttons for WALK, TABLE, GET, PUT */
  bar = gtk_hseparator_new ();
  gtk_box_pack_start (GTK_BOX (vbox), bar, FALSE, FALSE, 0);
  frame = gtk_frame_new (NULL);
  gtk_box_pack_start (GTK_BOX (vbox), frame, FALSE, FALSE, 0);
  button_box = gtk_hbutton_box_new ();
  gtk_container_border_width (GTK_CONTAINER (button_box), 2);
  gtk_button_box_set_layout (GTK_BUTTON_BOX (button_box), 
			     GTK_BUTTONBOX_SPREAD);
  gtk_button_box_set_spacing (GTK_BUTTON_BOX (button_box), 2);
  gtk_button_box_set_child_size (GTK_BUTTON_BOX (button_box), 85, 20);
  gtk_container_add (GTK_CONTAINER (frame), button_box);
  browser->wbutton = gtk_button_new_with_label (_("Walk"));
  gtk_container_add (GTK_CONTAINER (button_box), browser->wbutton);
  browser->tbutton = gtk_button_new_with_label (_("Table"));
  gtk_container_add (GTK_CONTAINER (button_box), browser->tbutton);
  gtk_widget_set_sensitive (browser->tbutton, FALSE);
  browser->gbutton = gtk_button_new_with_label (_("Get"));
  gtk_container_add (GTK_CONTAINER (button_box), browser->gbutton);
  gtk_widget_set_sensitive (browser->gbutton, FALSE);
  browser->pbutton = gtk_button_new_with_label (_("Put"));
  gtk_container_add (GTK_CONTAINER (button_box), browser->pbutton);
  gtk_widget_set_sensitive (browser->pbutton, FALSE);
/* Connect Signals */
  gtk_signal_connect (GTK_OBJECT (browser->wbutton), "clicked",
		      GTK_SIGNAL_FUNC (walk_button_cb),
		      browser);
  gtk_signal_connect (GTK_OBJECT (browser->tbutton), "clicked",
		      GTK_SIGNAL_FUNC (table_button_cb),
		      browser);
  gtk_signal_connect (GTK_OBJECT (browser->gbutton), "clicked",
		      GTK_SIGNAL_FUNC (get_button_cb),
		      browser);
  gtk_signal_connect (GTK_OBJECT (browser->pbutton), "clicked",
		      GTK_SIGNAL_FUNC (put_button_cb),
		      browser);
  gtk_signal_connect (GTK_OBJECT (browser->root), "select_row",
                      GTK_SIGNAL_FUNC (cb_select_row),
                      browser);
  gtk_signal_connect (GTK_OBJECT (browser->root), "tree_expand",
                      GTK_SIGNAL_FUNC (cb_expand),
                      browser);
  gtk_widget_show_all (hbox);
  if((node = smiGetNode(NULL, "ccitt")))
    {
      create_tree_item (GTK_WIDGET (browser), GTK_CTREE (browser->root), 
			NULL, node);
    }
  if((node = smiGetNode(NULL, "iso")))
    {
      create_tree_item (GTK_WIDGET (browser), GTK_CTREE (browser->root), 
			NULL, node);
    }
  if((node = smiGetNode(NULL, "joint-iso-ccitt")))
    {
      create_tree_item (GTK_WIDGET (browser), GTK_CTREE (browser->root), 
			NULL, node);
    }
  gtk_widget_set_sensitive (GTK_WIDGET (browser->root), TRUE);
  browser->request = NULL;
  browser->hostname = g_strdup("localhost");
  browser->rcom = g_strdup("public");
  browser->wcom = g_strdup("public");
  browser->normal = gtk_style_copy (browser->mib_value->style);
  browser->error = gtk_style_copy (browser->mib_value->style);
  gdk_color_parse ("#ff0000", &red);
  browser->error->fg[GTK_STATE_NORMAL] = red;
  D_FUNC_END;
}
/**
 * get_enums:
 * @browser: self
 * @node: SMI node to get the enums from
 *
 * Gets the enums of a given SMI node and inserts them into the description
 * field of this browser.
 *
 **/
static void
get_enums (GxSNMPMibBrowser *browser, SmiNode *node)
{
  char               buf[80];
  SmiNamedNumber    *num;
  SmiType           *type;
  D_FUNC_START;
  type = smiGetNodeType(node);
  if (type && type->basetype == SMI_BASETYPE_ENUM)
    {
      sprintf (buf, _("\n\nAvailable values:\n\n"));
      gtk_text_insert (GTK_TEXT_VIEW (browser->dpage), NULL, browser->normal->fg,
		       NULL, buf, strlen(buf));
      num = smiGetFirstNamedNumber(type);
      while (num)
	{
	  sprintf (buf, "%s (%d)\n", num->name, 
                   (int) num->value.value.integer32);
	  gtk_text_insert (GTK_TEXT_VIEW (browser->dpage), NULL, 
			   browser->normal->fg, NULL, buf, -1);
	  num = smiGetNextNamedNumber(num);
	}
    }
  D_FUNC_END;
}
/****************************************************************************
 * Object signals
 ***************************************************************************/
/****************************************************************************
 * Signals of other objects
 ***************************************************************************/
/**
 * mib_tree_drag_begin:
 * @wigdet: self
 * @context: drag context
 * @data: pointer to browser object
 *
 * SIGNAL Method to announce the begin of a drag operation.
 * 
 * The current focus of the MIB tree is saved in an instance variable of the
 * browser for a later drop operation.
 *
 **/
static void
mib_tree_drag_begin(GtkWidget* widget, GdkDragContext* context, gpointer data)
{
  GxSNMPMibBrowser *browser;
  D_FUNC_START;
  g_return_if_fail (data != NULL);
  browser = (GxSNMPMibBrowser *) data;
  browser->dragged_row = GTK_CLIST (browser->root)->focus_row;
  D_FUNC_END;
}
/**
 * mib_tree_data_get:
 * @wigdet: self
 * @context: drag context
 * @sel_data: target of DnD operation
 * @info: data format of DnD operarion
 * @dtime:
 * @data: pointer to browser object
 *
 * SIGNAL Method to announce the begin of a drag operation.
 * 
 * The current focus of the MIB tree is saved in an instance variable of the
 * browser for a later drop operation.
 *
 **/
static void
mib_tree_data_get(GtkWidget* widget, GdkDragContext* context,
		  GtkSelectionData* sel_data, guint info, guint dtime,
	 	  gpointer data)
{
  GxSNMPMibBrowser *browser;
  SmiNode          *node;
  SmiModule        *module;
  gchar            *buffer;
  D_FUNC_START;
  g_return_if_fail (data != NULL);
  browser = (GxSNMPMibBrowser *) data;
  node = gtk_clist_get_row_data (GTK_CLIST (browser->root), 
				 browser->dragged_row);
  module = smiGetNodeModule(node);
  switch (info)
    {
     case TARGET_STRING:
	buffer = (gchar *) g_malloc(1024);
	getoid(node, buffer, 1024);
	gtk_selection_data_set (sel_data, sel_data->target, 8, buffer,
				strlen(buffer));
	g_free(buffer);
	break;
      default:
	buffer = (gchar *) g_malloc(1024);
	snprintf(buffer, 1024, "%s.%s", module->name, node->name);
	gtk_selection_data_set (sel_data, sel_data->target, 8, buffer,
				strlen(buffer));
	g_free(buffer);
    }
  D_FUNC_END;
}
/**
 * cb_expand
 * @ctree: self
 * @node: node where event occurred
 * @data: pointer to browser object
 *
 * SIGNAL Method to announce expansion of an node object.
 *
 * This fills in the detail information from libsmi in case the dummy node is
 * found.
 *
 **/
static void
cb_expand (GtkCTree *ctree, GtkCTreeNode *node, gpointer data)
{
  GxSNMPMibBrowser *browser;
  SmiNode          *snode;
  SmiNode          *child;
  GtkCTreeRow      *crow;
  gchar            *buf;
  D_FUNC_START;
  g_return_if_fail (data != NULL);
  browser = (GxSNMPMibBrowser *) data;
  crow = GTK_CTREE_ROW(node);
  if (crow->children)
    {
      gtk_ctree_get_node_info(ctree, crow->children, &buf,
                        NULL, NULL, NULL, NULL, NULL, NULL, NULL);
      if (strcmp(buf, "!dummy!") == 0)
        {
	  gtk_clist_freeze(GTK_CLIST(ctree));
	  gtk_widget_set_sensitive(GTK_WIDGET(ctree), FALSE);
          gtk_ctree_remove_node(ctree, GTK_CTREE_NODE(crow->children));
	  snode = gtk_ctree_node_get_row_data(ctree, node);
	  if ((child = smiGetFirstChildNode(snode)))
	    {
	      while(child)
		{  
		  create_tree_item(GTK_WIDGET(browser), ctree, node, child);
		  child = smiGetNextChildNode(child);
		}
	    }
	  gtk_widget_set_sensitive(GTK_WIDGET(ctree), TRUE);
	  gtk_clist_thaw(GTK_CLIST(ctree));
        }
    }
  D_FUNC_END;
}
/**
 * cb_select_row:
 * @ctree: self
 * @row: row where event occurred
 * @col: column where event occurred
 * @e: type of event
 * @data: pointer to browser object
 *
 * SIGNAL Method to announce change of selection in a ctree.
 *
 * The browser is updated to reflect information of the new selected row.
 * This calls get_enums if necessary.
 *
 **/
static void 
cb_select_row(GtkCTree* ctree, gint row, gint col, GdkEvent *e, gpointer data) 
{
  GList          *selected;
  GtkCTreeNode   *selected_item;
  gint            nb_selected;
  guint           length;
  gint            num;
  SmiNode        *node;
  SmiModule      *module;
  SmiType        *type;
  gchar          *buffer;
  GxSNMPMibBrowser *browser;
  D_FUNC_START;
  g_return_if_fail (data != NULL);
  browser = (GxSNMPMibBrowser *) data;
  selected    = GTK_CLIST(ctree)->selection;
  nb_selected = g_list_length(selected);
  if (nb_selected != 1) 
    return;
  selected_item = GTK_CTREE_NODE(selected->data);
  node = gtk_ctree_node_get_row_data(ctree, selected_item);
  module = smiGetNodeModule(node);
  num = gtk_notebook_page_num(GTK_NOTEBOOK(browser->notebook), 
	browser->dpage->parent);
  gtk_notebook_set_page(GTK_NOTEBOOK(browser->notebook), num);
  gtk_text_freeze (GTK_TEXT_VIEW (browser->dpage));
  gtk_text_set_point(GTK_TEXT_VIEW (browser->dpage),0);
  length = gtk_text_get_length(GTK_TEXT_VIEW (browser->dpage));
  gtk_text_forward_delete(GTK_TEXT_VIEW (browser->dpage),length);
  if (node)
    {
      type = smiGetNodeType(node);
      if (!type)
        {
	  buffer = _("None");
	}
      else
        {
          buffer = match_strval (type->basetype, smi_basetype);
          if (!buffer)
	    {
	      g_warning("Unknown basetype found: %d", type->basetype);
	      buffer = _("Illegal");
	    }
	}
      gtk_entry_set_text (GTK_ENTRY (browser->mib_type), buffer);
      buffer = match_strval (node->status, smi_status);
      if (!buffer)
	{
	  g_warning("Unknown status found: %d", node->status);
	  buffer = _("Illegal");
	}
      gtk_entry_set_text (GTK_ENTRY (browser->mib_status), buffer);
      gtk_entry_set_text (GTK_ENTRY (browser->mib_module), module->name);
      buffer = (gchar *) g_malloc(1024);
      getoid(node, buffer, 1024);
      gtk_entry_set_text (GTK_ENTRY (browser->mib_oid), buffer);
      g_free(buffer);
      gtk_entry_set_text (GTK_ENTRY (browser->mib_label), node->name);
      if (node->description) 
	{
	  gtk_text_insert (GTK_TEXT_VIEW (browser->dpage), NULL, 
			  browser->normal->fg, NULL, node->description, 
			  strlen (node->description));
        }
      get_enums (browser, node);
      d_print (DEBUG_DUMP, "Selected node, kind == %d\n", node->nodekind);
      switch (node->nodekind)
	{
	  case SMI_NODEKIND_TABLE:
	    gtk_widget_set_sensitive (browser->tbutton, TRUE);
	    gtk_widget_set_sensitive (browser->gbutton, FALSE);
	    gtk_widget_set_sensitive (browser->pbutton, FALSE);
	    break;
	  case SMI_NODEKIND_SCALAR:
	    gtk_widget_set_sensitive (browser->tbutton, FALSE);
	    gtk_widget_set_sensitive (browser->gbutton, TRUE);
	    if (node->access == SMI_ACCESS_READ_WRITE)
	      gtk_widget_set_sensitive (browser->pbutton, TRUE);
	    else
	      gtk_widget_set_sensitive (browser->pbutton, FALSE);
	    break;
	  default:
	    gtk_widget_set_sensitive (browser->tbutton, FALSE);
	    gtk_widget_set_sensitive (browser->gbutton, FALSE);
	    gtk_widget_set_sensitive (browser->pbutton, FALSE);
     	    break;
	} 
    }
  gtk_text_thaw (GTK_TEXT_VIEW (browser->dpage));
  D_FUNC_END;
}
/**
 * table_button_cb:
 * @widget: self
 * @data: pointer to browser object
 *
 * SIGNAL Method to announce press of TABLE button
 *
 **/
static void
table_button_cb	(GtkWidget* widget, gpointer data)
{
  GList          *selected;
  GtkCTreeNode   *selected_item;
  gint            nb_selected;
  SmiNode        *node;
  GxSNMPMibBrowser *browser;
  D_FUNC_START;
  g_return_if_fail (data != NULL);
  browser = (GxSNMPMibBrowser *) data;
  selected    = GTK_CLIST(browser->root)->selection;
  nb_selected = g_list_length(selected);
  if (nb_selected != 1) 
    return;
  selected_item = GTK_CTREE_NODE(selected->data);
  node = gtk_ctree_node_get_row_data(GTK_CTREE(browser->root), selected_item);
  D_FUNC_END;
}
/**
 * suboid:
 * @baseoid: Base OID
 * @baselen: Base OID length
 * @oid: OID
 * @len: OID length
 *
 * returns TRUE, if OID is part of the Base OID subtree, else returns FALSE.
 *
 **/
static gboolean
suboid(guint *baseoid, guint baselen, gulong *oid, guint len)
{
  guint i;
  D_FUNC_START;
  if (len < baselen)
    {
      D_FUNC_END;
      return FALSE;
    }
  for (i=0; i<baselen; i++)
    {
      if (baseoid[i] != oid[i])
	{
	  D_FUNC_END;
	  return FALSE;
	}
    }
  D_FUNC_END;
  return TRUE;
}
/**
 * setup_host:
 * @widget: self
 * @host: pointer to host_snmp structure
 *
 * setup host_snmp structure from object data.
 *
 **/
void
setup_host (GxSNMPMibBrowser *browser, host_snmp *host)
{
  D_FUNC_START;
  host->domain        = AF_INET;
  host->rcomm         = browser->rcom;
  host->wcomm         = browser->wcom;
  host->retries       = 5;
  host->name          = browser->hostname;
  host->status        = 0;
  host->port          = 161;
  host->timeout       = 10;
  host->version       = SNMP_V1;
  host->done_callback = update_mib_value;
  host->time_callback = update_mib_timeout;
  host->magic         = browser;
  D_FUNC_END;
}
/**
 * update_mib_value (host_snmp *host, void *magic, SNMP_PDU *spdu, GSList *objs)
 * @host: host_snmp structure
 * @magic: data attached to callback
 * @spdu: SNMP PDU carrying variables
 * @objs: variables in SNMP object form
 *
 **/
gboolean
update_mib_value (host_snmp *host, void *magic, SNMP_PDU *spdu, GSList *objs)
{
  char    buf[1024], *ptr;
  struct _SNMP_OBJECT *obj;
  GxSNMPMibBrowser *browser;
  D_FUNC_START;
  g_return_val_if_fail (magic != NULL, TRUE);
  browser = (GxSNMPMibBrowser *) magic;
  browser->request = NULL;
  obj = objs->data;
  if (obj) g_snmp_printf (buf, sizeof(buf), obj);
  gtk_widget_set_style(browser->mib_value, browser->normal);
  if (spdu->request.error_status)
    {
      gtk_widget_set_style(browser->mib_value, browser->error);
      ptr = match_strval(spdu->request.error_status, snmp_error);
      if (ptr) strcpy(buf, ptr);
      else snprintf(buf, 1024, "unknown error %d", spdu->request.error_status);
    }
  gtk_entry_set_text (GTK_ENTRY (browser->mib_value), buf);
  D_FUNC_END;
  return TRUE;
}
/**
 * update_mib_timeout (host_snmp *host, void *magic)
 * @host: host_snmp structure
 * @magic: data attached to callback
 *
 **/
void
update_mib_timeout (host_snmp *host, void *magic)
{
  GxSNMPMibBrowser *browser;
  D_FUNC_START;
  g_return_if_fail (magic != NULL);
  browser = (GxSNMPMibBrowser *) magic;
  browser->request = NULL;
  gtk_widget_set_style(browser->mib_value, browser->error);
  gtk_entry_set_text (GTK_ENTRY (browser->mib_value), "timeout");
  D_FUNC_END;
}
/**
 * walk_mib_value (host_snmp *host, void *magic, SNMP_PDU *spdu, GSList *objs)
 * @host: host_snmp structure
 * @magic: data attached to callback
 * @spdu: SNMP PDU carrying variables
 * @objs: variables in SNMP object form
 *
 **/
gboolean
walk_mib_value (host_snmp *host, void *magic, SNMP_PDU *spdu, GSList *objs)
{
  char                 buf[1024],
                      *ptr;
  struct _SNMP_OBJECT *obj;
  guint                i;
  GSList              *nobjs;
  GxSNMPMibBrowser    *browser;
  gulong              *mib;
  guint                miblen;
  D_FUNC_START;
  g_return_val_if_fail (magic != NULL, TRUE);
  browser = (GxSNMPMibBrowser *) magic;
  browser->request = NULL;
  obj = objs->data;
  if (spdu->request.error_status)
    {
      ptr = match_strval(spdu->request.error_status, snmp_error);
      if (ptr) strcpy(buf, ptr);
      else snprintf(buf, 1024, "unknown error %d", spdu->request.error_status);
      gtk_text_insert (GTK_TEXT_VIEW (browser->wpage), NULL, browser->error->fg,
                       NULL, buf, strlen (buf));
    }
  else if (obj)
    {
      if (suboid(browser->baseoid, browser->baselen, obj->id, obj->id_len))
	{
          snprintf(buf, sizeof(buf), "%ld", obj->id[0]);
          for (i=1; i < obj->id_len; i++)
            snprintf(buf + strlen (buf), sizeof(buf) - strlen (buf), ".%ld",
                     obj->id[i]);
          snprintf(buf + strlen (buf), sizeof(buf) - strlen (buf), ": ");
          gtk_text_insert (GTK_TEXT_VIEW (browser->wpage), NULL, 
			   browser->normal->fg, NULL, buf, strlen (buf));
          g_snmp_printf (buf, sizeof(buf), obj);
          snprintf(buf + strlen (buf), sizeof(buf) - strlen (buf), "\n");
          gtk_text_insert (GTK_TEXT_VIEW (browser->wpage), NULL, 
			   browser->normal->fg, NULL, buf, strlen (buf));
          nobjs = NULL;
	  mib = g_malloc((obj->id_len) * sizeof(gulong));
          for (miblen = 0; miblen < obj->id_len; miblen++)
            mib[miblen] = obj->id[miblen];
          g_pdu_add_oid(&nobjs, mib, miblen, SNMP_NULL, NULL);
          browser->request = g_async_getnext(host, nobjs);
	}
    }
  D_FUNC_END;
  return TRUE;
}
/**
 * walk_mib_timeout (host_snmp *host, void *magic)
 * @host: host_snmp structure
 * @magic: data attached to callback
 *
 **/
void
walk_mib_timeout (host_snmp *host, void *magic)
{
  GxSNMPMibBrowser *browser;
  guchar *error;
  D_FUNC_START;
  g_return_if_fail (magic != NULL);
  browser = (GxSNMPMibBrowser *) magic;
  browser->request = NULL;
  error="*timeout*";
  gtk_text_insert (GTK_TEXT_VIEW (browser->wpage), NULL, browser->error->fg,
                   NULL, error, strlen (error));
  D_FUNC_END;
}
/**
 * get_button_cb:
 * @widget: self
 * @data: pointer to browser object
 *
 * SIGNAL Method to announce press of GET button
 *
 **/
static void
get_button_cb (GtkWidget* widget, gpointer data)
{
  GList            *selected;
  GtkCTreeNode     *selected_item;
  gint              nb_selected;
  gint	  	    instance[1];
  SmiNode          *node;
  GxSNMPMibBrowser *browser;
  GSList           *objs;
  D_FUNC_START;
  g_return_if_fail (data != NULL);
  browser = (GxSNMPMibBrowser *) data;
  selected    = GTK_CLIST(browser->root)->selection;
  nb_selected = g_list_length(selected);
  if (nb_selected != 1) 
    return;
  selected_item = GTK_CTREE_NODE(selected->data);
  node = gtk_ctree_node_get_row_data(GTK_CTREE(browser->root), selected_item);
  if (browser->request)
    g_remove_request(browser->request);
  setup_host(browser,&shost);
  objs = NULL;
  instance[0] = 0;
  add_oid(&objs, node->oid, node->oidlen, instance, 1, SNMP_NULL, NULL);
  browser->request = g_async_get(&shost, objs);
  D_FUNC_END;
}
/**
 * put_button_cb:
 * @widget: self
 * @data: pointer to browser object
 *
 * SIGNAL Method to announce press of PUT button
 *
 **/
static void
put_button_cb (GtkWidget* widget, gpointer data)
{
  GList          *selected;
  GtkCTreeNode   *selected_item;
  gint            nb_selected;
  gint		  instance[1];
  SmiNode        *node;
  SmiType        *type;
  GxSNMPMibBrowser *browser;
  GSList         *objs;
  char		 *value;
  guint32	  val;
  D_FUNC_START;
  g_return_if_fail (data != NULL);
  browser = (GxSNMPMibBrowser *) data;
  selected    = GTK_CLIST(browser->root)->selection;
  nb_selected = g_list_length(selected);
  if (nb_selected != 1) 
    return;
  selected_item = GTK_CTREE_NODE(selected->data);
  node = gtk_ctree_node_get_row_data(GTK_CTREE(browser->root), selected_item);
  type = smiGetNodeType(node);
  if (browser->request)
    g_remove_request(browser->request);
  setup_host(browser,&shost);
  objs = NULL;
  instance[0] = 0;
  value = gtk_entry_get_text(GTK_ENTRY(browser->mib_value));
  switch(type->basetype)
    {
      case SMI_BASETYPE_UNKNOWN:
      case SMI_BASETYPE_INTEGER64:
      case SMI_BASETYPE_FLOAT32:
      case SMI_BASETYPE_FLOAT64:
      case SMI_BASETYPE_FLOAT128:
      case SMI_BASETYPE_BITS:
      case SMI_BASETYPE_OBJECTIDENTIFIER:  /* XXX */
	/* Not supported in the moment */
	D_FUNC_END;
	return;
	break;
      case SMI_BASETYPE_INTEGER32:
      case SMI_BASETYPE_UNSIGNED32:
      case SMI_BASETYPE_UNSIGNED64:
      case SMI_BASETYPE_ENUM:  /* XXX */
	val = atoi(value);
	add_oid(&objs, node->oid, node->oidlen, instance, 1, SNMP_INTEGER, 
	  &val);
	break;
      case SMI_BASETYPE_OCTETSTRING:
	add_oid(&objs, node->oid, node->oidlen, instance, 1, SNMP_OCTETSTR, 
	  value);
	break;
    }
  browser->request = g_async_set(&shost, objs);
  D_FUNC_END;
}
/**
 * walk_button_cb:
 * @widget: self
 * @data: pointer to browser object
 *
 * SIGNAL Method to announce press of WALK button
 * starts to perform SNMP GETNEXT requests on the subtree of the currently
 * selected OID.
 *
 **/
static void
walk_button_cb (GtkWidget* widget, gpointer data)
{
  GList            *selected;
  GtkCTreeNode     *selected_item;
  guint             length;
  gint		    num;
  gint              nb_selected;
  gint		    instance[1];
  SmiNode          *node;
  GxSNMPMibBrowser *browser;
  GSList           *objs;
  D_FUNC_START;
  g_return_if_fail (data != NULL);
  browser = (GxSNMPMibBrowser *) data;
  selected    = GTK_CLIST(browser->root)->selection;
  nb_selected = g_list_length(selected);
  if (nb_selected != 1) 
    return;
  selected_item = GTK_CTREE_NODE(selected->data);
  node = gtk_ctree_node_get_row_data(GTK_CTREE(browser->root), selected_item);
/* Start request */
  if (browser->request)
    g_remove_request(browser->request);
  setup_host(browser,&shost);
  shost.done_callback = walk_mib_value;
  shost.time_callback = walk_mib_timeout;
  objs = NULL;
  add_oid(&objs, node->oid, node->oidlen, instance, 0, SNMP_NULL, NULL);
  browser->baseoid = node->oid;
  browser->baselen = node->oidlen;
  gtk_text_set_point(GTK_TEXT_VIEW (browser->wpage),0);
  length = gtk_text_get_length(GTK_TEXT_VIEW (browser->wpage));
  gtk_text_forward_delete(GTK_TEXT_VIEW (browser->wpage),length);
  num = gtk_notebook_page_num(GTK_NOTEBOOK(browser->notebook), 
	browser->wpage->parent);
  gtk_notebook_set_page(GTK_NOTEBOOK(browser->notebook), num);
  browser->request = g_async_getnext(&shost, objs);
  D_FUNC_END;
}
/****************************************************************************
 * Public API
 ***************************************************************************/
/**
 * gxsnmp_mib_browser_new:
 *
 * Creates new MIB browser object
 *
 * Return value: MIB browser object
 **/
GtkWidget *
gxsnmp_mib_browser_new ()
{
  GxSNMPMibBrowser  *browser;
  D_FUNC_START;
  browser = gtk_type_new (gxsnmp_mib_browser_get_type ());
  D_FUNC_END;
  return GTK_WIDGET (browser);
}
