/* -*- Mode: C -*-
 * $Id$
 * GXSNMP - An snmp managment application
 * Copyright (C) 1998 - 2001 Gregory McLean, Jochen Friedrich
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc.,  59 Temple Place - Suite 330, Cambridge, MA 02139, USA.
 *
 * The mib browser control.
 */
#ifdef HAVE_CONFIG_H
#include <config.h>
#endif

#include <gnome.h>

#include <bonobo.h>

#include "gxsnmp_mib_browser.h"
#include "gxsnmp_util.h"

#include "debug.h"

static int object_count = 0;

enum {
  PROP_VALUE,
  PROP_HOSTNAME,
  PROP_RCOM,
  PROP_WCOM
};

/****************************************************************************
 * Bonobo functions
 ***************************************************************************/
static void
set_prop (BonoboPropertyBag *bag, const BonoboArg *arg, guint arg_id,
	  CORBA_Environment *ev, gpointer user_data)
{
  GxSNMPMibBrowser *control;
  D_FUNC_START;
  g_return_if_fail (user_data != NULL);
  control = (GxSNMPMibBrowser *) user_data;
  switch (arg_id)
    {
      case PROP_VALUE: 
	gtk_widget_set_style(control->mib_value, control->normal);
	gtk_entry_set_text(control->mib_value, BONOBO_ARG_GET_STRING (arg));
	break;
      case PROP_HOSTNAME:
	g_free(control->hostname);
	control->hostname = g_strdup(BONOBO_ARG_GET_STRING (arg));
	break;
      case PROP_RCOM:
	g_free(control->rcom);
	control->rcom = g_strdup(BONOBO_ARG_GET_STRING (arg));
	break;
      case PROP_WCOM:
	g_free(control->wcom);
	control->wcom = g_strdup(BONOBO_ARG_GET_STRING (arg));
	break;
    }
  D_FUNC_END;
}
static void
get_prop (BonoboPropertyBag *bag, BonoboArg *arg, guint arg_id, 
	  CORBA_Environment *ev, gpointer user_data)
{
  GxSNMPMibBrowser *control;
  D_FUNC_START;
  g_return_if_fail (user_data != NULL);
  control = (GxSNMPMibBrowser *) user_data;
  switch (arg_id)
    {
      case PROP_VALUE: 
	BONOBO_ARG_SET_STRING (arg, gtk_entry_get_text(control->mib_value));
	break;
      case PROP_HOSTNAME:
	BONOBO_ARG_SET_STRING (arg, control->hostname);
	break;
      case PROP_RCOM:
	BONOBO_ARG_SET_STRING (arg, control->rcom);
	break;
      case PROP_WCOM:
	BONOBO_ARG_SET_STRING (arg, control->wcom);
	break;
    }
  D_FUNC_END;
}
static void
gxsnmp_mib_browser_destroyed(GtkObject *obj)
{
  object_count--;
  if (object_count <= 0) {
    gtk_main_quit ();
  }
}
static BonoboObject *
gxsnmp_mib_browser_factory (BonoboGenericFactory *Factory, void *closure)
{
  BonoboPropertyBag *pb;
  BonoboControl     *control;
  GtkWidget         *browser;

  D_FUNC_START;
  browser = gxsnmp_mib_browser_new ();
  gtk_widget_show (browser);

  /* Create the control. */
  control = bonobo_control_new (browser);
  pb = bonobo_property_bag_new (get_prop, set_prop, browser);
  bonobo_control_set_properties (control, pb, NULL);
  bonobo_property_bag_add (pb, "value", PROP_VALUE, BONOBO_ARG_STRING, 
    NULL, "SNMP Value", 0);
  bonobo_property_bag_add (pb, "hostname", PROP_HOSTNAME, BONOBO_ARG_STRING,
    NULL, "Hostname", 0);
  bonobo_property_bag_add (pb, "rcom", PROP_RCOM, BONOBO_ARG_STRING,
    NULL, "Read Community Name", 0);
  bonobo_property_bag_add (pb, "wcom", PROP_WCOM, BONOBO_ARG_STRING,
    NULL, "Write Community Name", 0);
  bonobo_control_set_automerge (control, TRUE);
  object_count++;
  gtk_signal_connect (GTK_OBJECT (browser), "destroy",
     gxsnmp_mib_browser_destroyed, NULL);  
  D_FUNC_END;
  return BONOBO_OBJECT (control);
}

void
gxsnmp_mib_browser_factory_init (void)
{
  static BonoboGenericFactory *mib_browser_control_factory = NULL;

  D_FUNC_START;
  if (mib_browser_control_factory != NULL)
    return;
  mib_browser_control_factory = bonobo_generic_factory_new (
    "OAFIID:gxsnmp-mib_browser-factory:0cd65307-0f36-4966-bb21-c2d76a38ffca",
    gxsnmp_mib_browser_factory, NULL);
  if (mib_browser_control_factory == NULL)
    g_error ("I could not register a MIB browser factory.");
  D_FUNC_END;
}

/* EOF */
