/*
 * gxsnmp_mib_browser_main.c
 * based on sample-control-container.c
 * 
 * Authors:
 *   Nat Friedman  (nat@helixcode.com)
 *   Michael Meeks (michael@helixcode.com)
 *
 * Copyright 1999, 2000 Helix Code, Inc.
 */
#include <config.h>
#include <gnome.h>
#include <smi.h>

#include "gxsnmp_mib_browser.h"

static gchar    *hname = NULL;
static gchar    *rcom  = NULL;
static gchar    *wcom  = NULL;
poptContext      pctx;

struct poptOption options[] = {
  { "hostname", '\0', POPT_ARG_STRING, &hname, 0,
    N_("Hostname to be used for MIB browser control"), "HOSTNAME" },
  { "rcom", '\0', POPT_ARG_STRING, &rcom, 0,
    N_("Read community name to be used for MIB browser control"), "RCOM" },
  { "wcom", '\0', POPT_ARG_STRING, &wcom, 0,
    N_("Write community name to be used for MIB browser control"), "WCOM" },
  { NULL, '\0', 0, NULL, 0 }
};

static void
quit_button_cb (GtkWidget *widget, gpointer  data)
{
  gtk_main_quit();
}

int
main (int argc, char **argv)
{
  GxSNMPMibBrowser *browser;
  GnomeApp         *window;

  bindtextdomain (PACKAGE, GNOMELOCALEDIR);
  textdomain (PACKAGE);
  gnome_init_with_popt_table ("gxsnmp-mib_browser", "1.0", 
    argc, argv, options, 0, &pctx);
  poptFreeContext (pctx);

  smiInit("gxsnmp");
  if (!g_snmp_init (FALSE))
    g_error ("Failed to initialize the SNMP library.");

  window = gnome_app_new ("gxsnmp_mib_browser", "GxSNMP MIB Browser");

  browser = gxsnmp_mib_browser_new ();
  gtk_widget_show (browser);

  gnome_app_set_contents (GNOME_APP (window), browser);
  gtk_widget_show (window);

  if (hname)
    {
      g_free(browser->hostname);
      browser->hostname = g_strdup(hname);
    }  
  if (rcom)
    {
      g_free(browser->rcom);
      browser->rcom = g_strdup(rcom);
    }  
  if (wcom)
    {
      g_free(browser->wcom);
      browser->wcom = g_strdup(wcom);
    }

  gtk_signal_connect (GTK_OBJECT (window), "destroy",
    (GtkSignalFunc) quit_button_cb, NULL);

  gtk_main ();

  return 0;
}
