/*
 * GXSNMP - An snmp managment application
 * Copyright (C) 2000,2001 Larry Liimatainen
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc.,  59 Temple Place - Suite 330, Cambridge, MA 02139, USA.
 *
 * util.c -- mixed add on features
 */

/*
 * utility functions for the gxevents
 *
 */

#include <sys/wait.h>
#include <netinet/in.h>


#include <gnome.h>
#include <libbonobo.h>
#include "orbit-gxsnmp-db.h"
#include "dbapi.h"
#include "gxsnmp/gxsnmp_dbapi.h"
#include "dae.h"
#include "gxevents_util.h"

extern GxSNMPEventDisplay * control;

/* menutools_interpret()
 * execute a command from tools menu
 */
void
menutools_interpret(GtkWidget *mti, gchar *text)
{
  pid_t pid;
  gchar *command;

  command = g_strsub_substitute(control->strsub, text);
  if(!(pid = fork())){
    if(!(fork())) system(command);
      _exit(0);
    }
  waitpid(pid,NULL,0);
  return;

}

/* tvw_close()
* Close a Tiny View Window
* and release any memory
*/

static void
tvw_close(GtkButton *tvw, GtkWidget *tvw_win)
{
  gtk_widget_destroy(tvw_win);
}

void gui_simple_label(GtkWidget *parent, gchar *label)
{
  GtkWidget *foo;
  foo = gtk_label_new(label);
  gtk_misc_set_alignment (GTK_MISC (foo), 0, 0);
  gtk_box_pack_start (GTK_BOX (parent), foo, FALSE, FALSE, 0);
  gtk_widget_show(foo);
}

/* tvw_open()
 *
 * send selected trap to a own window
 */
void
tvw_open()
{
GtkWidget *tvw_win, *vbox, *foo;
gchar line[400];
gxsnmp_event * event;
g_trap *trap;
gchar *txt;

  event = &control->selected_event;
  trap = &control->selected_event.data;

  tvw_win = gtk_window_new(GTK_WINDOW_TOPLEVEL);
  vbox = gtk_vbox_new (FALSE, 4);
  gtk_container_add(GTK_CONTAINER(tvw_win), vbox);
  gtk_widget_show(vbox);

  g_snprintf(line,400,"Date: %s", ctime((time_t *)& event->timestamp));
  gui_simple_label(vbox, line);

  g_snprintf(line,400,"Category: %d",event->category);
  gui_simple_label(vbox, line);

  g_snprintf(line,400,"Severity: %d",event->severity);
  gui_simple_label(vbox, line);

  g_snprintf(line,400,"Source: %s", event->source);
  gui_simple_label(vbox, line);

  g_snprintf(line,400,"Summary: %s", event->summary);
  gui_simple_label(vbox, line);

  g_snprintf(line,400,"Description: %s", event->description);
  gui_simple_label(vbox, line);
  
  switch(event->type){
    case GXSNMP_EVENT_SNMP_TRAP:
      g_snprintf(line,400,"Name: %s",trap->name);
      gui_simple_label(vbox, line);
      g_snprintf(line,400,"OID: %s",trap->oid);
      gui_simple_label(vbox, line);
//      g_snprintf(line,400,"Translated: %s",trap->noid);
//      gui_simple_label(vbox, line);
      g_snprintf(line,400,"Generic: %u",trap->g);
      gui_simple_label(vbox, line);
      g_snprintf(line,400,"Specific: %u",trap->s);
      gui_simple_label(vbox, line);
      g_snprintf(line,400,"FLAGS: %u",trap->flag);
      gui_simple_label(vbox, line);

      txt = g_trap_arg_to_string(trap);
      gui_simple_label(vbox, txt);
      g_free(txt);
    
    break;
    default:
      gui_simple_label(vbox, "Unknown event type");
    break;
  }

  foo = gtk_button_new_with_label("Dismiss");
  gtk_signal_connect(GTK_OBJECT(foo), "clicked",GTK_SIGNAL_FUNC(tvw_close), tvw_win);
  gtk_misc_set_alignment (GTK_MISC (foo), 0, 0);
  gtk_box_pack_start (GTK_BOX (vbox), foo, FALSE, FALSE, 0);
  gtk_widget_show(foo);

  gtk_widget_show_all(tvw_win);
}

/* sub_close()
 *
 * 
 *
 */
void
sub_close(GtkWidget *win)
/*I wonder if win is the dismiss button widget, then for(;;) beneith is unecessery*/
{ 
GList *swgl;
sub_window *subwin;

  swgl = control->sub_windows;
  while(swgl){
    subwin = swgl->data;
    if(subwin->win == win){
      gtk_widget_destroy(win);
      subwin->stat = SUBWIN_EMPTY;   /*this slot is set to empty, for reusage*/
      g_free(subwin->acl);
    }
    swgl = swgl->next;
  }
}

/* sub_open()
 *
 * open a sub trap display
 */
void
sub_open(sub_window *subw)
{
GtkWidget *sub_vbox;
GtkWidget *foo;
GtkWidget *mywin;

  subw->win = gtk_window_new(GTK_WINDOW_TOPLEVEL);
  sub_vbox = gtk_vbox_new (FALSE, 4);
  gtk_container_add(GTK_CONTAINER(subw->win), sub_vbox);
  gtk_widget_show(sub_vbox);

  foo = gtk_label_new("Loaded filter: test");
  gtk_box_pack_start (GTK_BOX (sub_vbox), foo, FALSE, FALSE, 0);

  subw->sublist = gtk_clist_new(1);
  /*gtk_signal_connect(GTK_OBJECT(subw->sublist), "select_row",GTK_SIGNAL_FUNC(xxx_cb),NULL);*/
  gtk_clist_set_selection_mode(GTK_CLIST(subw->sublist),GTK_SELECTION_SINGLE);
  gtk_clist_column_titles_hide(GTK_CLIST(subw->sublist));
  gtk_clist_set_column_width(GTK_CLIST(subw->sublist),0,200);

  mywin = gtk_scrolled_window_new(0, 0);
  gtk_scrolled_window_set_policy (GTK_SCROLLED_WINDOW (mywin),  GTK_POLICY_ALWAYS,  GTK_POLICY_AUTOMATIC);
  gtk_container_add (GTK_CONTAINER (mywin), subw->sublist);
  gtk_box_pack_start (GTK_BOX (sub_vbox), mywin, FALSE, FALSE, 0);

  foo = gtk_button_new_with_label("Dismiss");
  gtk_signal_connect(GTK_OBJECT(foo), "clicked",GTK_SIGNAL_FUNC(sub_close),NULL);
  gtk_box_pack_start (GTK_BOX (sub_vbox), foo, FALSE, FALSE, 0);
  gtk_widget_show(foo);

  gtk_widget_show_all(subw->win);
}

/* prune_trap()
 *
 * this callback will prune the selected trap.
 * add it into current loaded ACL
 */
void
prune_trap()
{ 
aclist acl;
g_trap *trap;

  trap = control->selected_event.data;
  if(control->aclfile[0] == 0) return;                 /*no trapfilter loaded*/
  acl.action = 13;                            /*add 'deny oid+g+s'*/
  acl.g = trap->g;
  acl.s = trap->s;
  strcpy(acl.oid, trap->oid);
  acl_add_filter(acl, control->aclfile);
  /*reload trapfilter*/
  if(control->trapfilter) g_free(control->trapfilter);
  else fprintf(stderr,"APP_ERR: no trapfilter loaded, but aclfile is set\n");
  control->trapfilter = load_acl(control->aclfile);             /*load event display filter (trapfilter )*/
}

/* ACCESS CONTROL LIST SECTION */

/*shows up the ACL file selection box*/
void fileacl_select()
{  
  gtk_widget_show_all(control->fileacl);
}

/*hides ACL file selection box*/
void fileacl_hide()
{  
  gtk_widget_hide(control->fileacl);
}

/*only used by main trap display */
void fileacl_load(GtkWidget *w, GtkFileSelection *fs)
{ 
  strcpy(control->aclfile,gtk_file_selection_get_filename(GTK_FILE_SELECTION(fs)));
  if(control->trapfilter) free(control->trapfilter);             /*free old trapfilter*/
  control->trapfilter = load_acl(control->aclfile);              /*load event display filter*/
  fileacl_hide();
}


/* SUB WINDOW SECTION */

/*shows up the ACL file selection box*/
void subfileacl_select()
{
  gtk_widget_show_all(control->subfileacl);
}

/*hides ACL file selection box*/
void subfileacl_hide()
{  
  gtk_widget_hide(control->subfileacl);
}

/*creates a new sub_window slot and loads filter */ 
void subfileacl_load(GtkWidget *w, GtkFileSelection *fs)
{ 
  sub_window *subwin;

  subfileacl_hide();                                  /*thank you, selection box*/
  subwin = g_malloc0(sizeof(sub_window));
  strcpy(control->aclfile,gtk_file_selection_get_filename(GTK_FILE_SELECTION(fs)));  
     /*retreive what access control 
       file to use file to load*/
  subwin->acl = load_acl(control->aclfile);            /*load sub windows filter*/
  subwin->stat = SUBWIN_ACTIVE;               /*set this sub window to state: used*/ 
  control->sub_windows = g_list_append(control->sub_windows, subwin);
  sub_open(subwin);                            /*create widgets*/
}

