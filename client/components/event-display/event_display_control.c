#include <gnome.h>
#include <netinet/in.h>

#include <libxml/tree.h>
#include <libxml/parser.h>

#include <libbonobo.h>
#include "orbit-gxsnmp-db.h"
#include "dae.h"
#include "gxevents_util.h"

/****** PROTOTYPES *************/
       void quit_cb          ();
       void notebook_page_cb (GtkNotebook *notebook, 
                              GtkNotebookPage *page, 
                              gint page_num, gpointer user);
       void eventlist_cb     (GtkWidget *eventlist, gint row, 
                              gint column, GdkEventButton *event);

       void fileacl_select   ();
       void fileacl_hide     ();
       void fileacl_load     (GtkWidget *, GtkFileSelection *);
       void subfileacl_select();
       void subfileacl_hide  ();
       void subfileacl_load  (GtkWidget *, GtkFileSelection *);

       void event_display_class_init (GxSNMPEventDisplayClass *klass);

       void evdpysrvd_connect (GtkWidget *);
       void evdpysrvd_disconnect (GtkWidget *);

/****** ********** *************/

/**** GUI *********************/
static GnomeUIInfo file_menu[] = {
  { GNOME_APP_UI_ITEM, N_("Save"), NULL,
    NULL, NULL, NULL,
    GNOME_APP_PIXMAP_STOCK, GNOME_STOCK_MENU_SAVE, 0, 0, NULL },

  { GNOME_APP_UI_ITEM, N_("Save as..."), NULL,
    NULL, NULL, NULL,
    GNOME_APP_PIXMAP_STOCK, GNOME_STOCK_MENU_BLANK, 0, 0, NULL },

  { GNOME_APP_UI_SEPARATOR },

  { GNOME_APP_UI_ITEM, N_("Print"), NULL,
    NULL, NULL, NULL,
    GNOME_APP_PIXMAP_STOCK, GNOME_STOCK_MENU_PRINT, 0, 0, NULL },

  { GNOME_APP_UI_SEPARATOR },

  { GNOME_APP_UI_ITEM, N_("Connect"), NULL, evdpysrvd_connect,
    NULL, NULL,
    GNOME_APP_PIXMAP_STOCK, GNOME_STOCK_MENU_BLANK, 0, 0, NULL },

  { GNOME_APP_UI_ITEM, N_("Disconnect"), NULL, evdpysrvd_disconnect,
    NULL, NULL,
    GNOME_APP_PIXMAP_STOCK, GNOME_STOCK_MENU_BLANK, 0, 0, NULL },

  { GNOME_APP_UI_SEPARATOR },

  { GNOME_APP_UI_ITEM, N_("Exit"), NULL, quit_cb,
    NULL, NULL,
    GNOME_APP_PIXMAP_STOCK, GNOME_STOCK_MENU_BLANK, 0, 0, NULL },

  { GNOME_APP_UI_ENDOFINFO },
};

static GnomeUIInfo edit_menu[] = {
  { GNOME_APP_UI_ITEM, N_("Load filter"), NULL,
    fileacl_select, NULL, NULL,
    GNOME_APP_PIXMAP_STOCK, GNOME_STOCK_MENU_BLANK, 0, 0, NULL },

  { GNOME_APP_UI_ITEM, N_("Prune trap"), NULL,
    prune_trap, NULL, NULL,
    GNOME_APP_PIXMAP_STOCK, GNOME_STOCK_MENU_BLANK, 0, 0, NULL },

  { GNOME_APP_UI_ITEM, N_("View trap"), NULL,
    tvw_open, NULL, NULL,
    GNOME_APP_PIXMAP_STOCK, GNOME_STOCK_MENU_BLANK, 0, 0, NULL },

  { GNOME_APP_UI_ITEM, N_("Open filtered trap window"), NULL,
    subfileacl_select, NULL, NULL,
    GNOME_APP_PIXMAP_STOCK, GNOME_STOCK_MENU_BLANK, 0, 0, NULL },

  { GNOME_APP_UI_ENDOFINFO },
};
static GnomeUIInfo *tools_menu;                                 
      /*run-time loaded from gxevents.conf, numerical is wc -l gxevents.conf*/

GnomeUIInfo event_panel_menu[] = {
  { GNOME_APP_UI_SUBTREE, N_("File"), NULL, file_menu, NULL, NULL,
    GNOME_APP_PIXMAP_NONE, NULL, 0, 0, NULL },
  { GNOME_APP_UI_SUBTREE, N_("Edit"), NULL, edit_menu, NULL, NULL,
    GNOME_APP_PIXMAP_NONE, NULL, 0, 0, NULL },
  { GNOME_APP_UI_SUBTREE, N_("Tools"), NULL, 0, NULL, NULL,             
        /*slot 2 MUST be Tools menu, moreinfo is overwritten when loading gxevents.conf*/                                    
    GNOME_APP_PIXMAP_NONE, NULL, 0, 0, NULL },                                                
  { GNOME_APP_UI_ENDOFINFO },
};

static GnomeUIInfo tools_menu_fail;
       /*if load of gxevents.conf fails, set Tools menu to empty*/
GnomeUIInfo *toolsmenu;
       /* if load success, we alloc space and fill her up */

/**** GXEVENT MENU *****/

menutoollist *menutools;        /* Dynamically loaded tools */





/****************************************************************************
 * Class registration
 ***************************************************************************/
/** 
 * event_display_get_type:
 *
 * Registers class with GTK and returns handle ("type") of class. The handle
 * is cached in a static class variable for repeated calls of this function.
 *
 * Return value: unique type
 **/
GtkType
event_display_get_type ()
{
  static GtkType list_type = 0;
  if (!list_type)
    {
      GtkTypeInfo list_info =
      { 
        "GxSNMPEventDisplay",
        sizeof (GxSNMPEventDisplay),
        sizeof (GxSNMPEventDisplayClass),
        (GtkClassInitFunc) event_display_class_init,
        (GtkObjectInitFunc) event_display_init,
        /* reserved 1 */ NULL,
        /* reserved 2 */ NULL,
        (GtkClassInitFunc) NULL,
      };
      list_type = gtk_type_unique(gtk_vbox_get_type(), &list_info);
    }
  return list_type;
}   

/****************************************************************************
 * Class methods
 ***************************************************************************/
/** 
 * event_display_class_init:
 * @klass: self
 *  
 * Constructor of this class.
 *  
 **/
void
event_display_class_init (GxSNMPEventDisplayClass *klass)
{   
}   

void
event_display_init (GxSNMPEventDisplay *control)
{   
  gchar line[2000];
  gchar line2[2000];
  gchar foo[2000];
  gint i,a, w_size;
  GtkWidget *vbox1;
  GtkWidget *menu;
  GtkWidget *statusbar;
  xmlDocPtr doc;
  xmlNodePtr node;
  gchar *menu_name, *menu_cmd;

  control->black.pixel = 0x0000;
  control->black.red =   0x0000;
  control->black.green = 0x0000;
  control->black.blue =  0x0000;

  control->red.pixel = 0x0000;
  control->red.red =   0xffff;
  control->red.green = 0x0000;
  control->red.blue =  0x0000;

  control->yellow.pixel = 0x0000;
  control->yellow.red =   0xffff;
  control->yellow.green = 0xffff;
  control->yellow.blue =  0x0000;

  control->amber.pixel = 0x0000;
  control->amber.red =   0xafff;
  control->amber.green = 0xffff;
  control->amber.blue =  0x0000;

  control->green.pixel = 0x0000;
  control->green.red =   0x0000;
  control->green.green = 0xffff;
  control->green.blue =  0x0000;

  control->white.pixel = 0x0000;
  control->white.red =   0xffff;
  control->white.green = 0xffff;
  control->white.blue =  0xffff;

  control->grey.pixel = 0x0000;
  control->grey.red =   0xafff;
  control->grey.green = 0xafff;
  control->grey.blue =  0xafff;

  control->sub_windows = NULL;
  control->ddchannel = 0;                 /*if ddsock is zero, we can't use database*/
  control->trapid = 0;
  control->event_list = NULL;
  control->trapfilter = 0;
  control->notebook_cnt = 0;
  control->env.traptab = NULL;
  control->evdpys = NULL;
  control->strsub = NULL;

/*********/

  control->window = gtk_window_new (GTK_WINDOW_TOPLEVEL);
  gtk_object_set_data (GTK_OBJECT (control->window), "window", control->window);
  gtk_window_set_title (GTK_WINDOW (control->window), _("window"));

  vbox1 = gtk_vbox_new (FALSE, 0);
  gtk_widget_ref (vbox1);
  gtk_object_set_data_full (GTK_OBJECT (control->window), "vbox1", vbox1,
                            (GtkDestroyNotify) gtk_widget_unref);

  gtk_widget_show (vbox1);
  gtk_container_add (GTK_CONTAINER (control->window), vbox1);
  
/****** Create MENU *********/
/***** RUN-TIME LOAD THE TOOLS_MENU ********/
  
  tools_menu_fail.type = GNOME_APP_UI_ENDOFINFO;
  
  strcpy(line,control->env.base);
  strcat(line,"/conf/gxevents.conf");
  doc = xmlParseFile(line);

  if(doc == NULL){
    fprintf(stderr,"GXEVENTS.CONF LOAD ERROR\n");
    fprintf(stderr,"WHILE TRYING TO LOAD FILE: %s\n", line);
    event_panel_menu[2].moreinfo = &tools_menu_fail;
  }

  node = xmlDocGetRootElement(doc);
  if(node == NULL){
    fprintf(stderr,"GXEVENTS.CONF SEEMS EMPTY\n");
    xmlFreeDoc(doc);
    event_panel_menu[2].moreinfo = &tools_menu_fail;
  }
  if(xmlStrcmp(node->name, (const xmlChar *) "Menu")){
    fprintf(stderr,"GXEVENTS.CONF FILE SYNTAX ERROR\n");
    xmlFreeDoc(doc);
    event_panel_menu[2].moreinfo = &tools_menu_fail;
  }

  /***** we need to determine the number of items to alloc */
  if (!(toolsmenu = malloc(10 * sizeof(GnomeUIInfo)))){
  }
  if (!(menutools = malloc(10 * sizeof(menutoollist)))){
  }

  a = 0;
  for(node; node != NULL; node = node->next){
    
    menu_name = xmlGetProp(node, "name");
    menu_cmd  = xmlGetProp(node, "command");
    if(!menu_name || !menu_cmd) continue;

    strcpy(menutools[a].name, menu_name);               /*set table that interpreter uses*/
    strcpy(menutools[a].cmd, menu_cmd);

    toolsmenu[a].type = GNOME_APP_UI_ITEM;
    toolsmenu[a].label = menutools[a].name;
    toolsmenu[a].hint = NULL;
    toolsmenu[a].moreinfo = menutools_interpret;
    toolsmenu[a].user_data = menutools[a].cmd;    
                /*bind command to set as argument to menutools_interpret()*/
    toolsmenu[a].unused_data = NULL;
    toolsmenu[a].pixmap_type = GNOME_APP_PIXMAP_STOCK;
    toolsmenu[a].pixmap_info = GNOME_STOCK_MENU_BLANK;
    toolsmenu[a].accelerator_key = 0;
    toolsmenu[a].ac_mods = 0;
    toolsmenu[a].widget = NULL;
    a++;
  }

  toolsmenu[a].type = GNOME_APP_UI_ENDOFINFO;
  tools_menu = toolsmenu;
  event_panel_menu[2].moreinfo = tools_menu;

  menu = gtk_menu_bar_new ();
  gtk_widget_ref (menu);
  gtk_object_set_data_full (GTK_OBJECT (control->window), "menu", menu,
                            (GtkDestroyNotify) gtk_widget_unref);
  
  gtk_widget_show (menu);
  gtk_box_pack_start (GTK_BOX (vbox1), menu, FALSE, FALSE, 0);
  gnome_app_fill_menu (GTK_MENU_SHELL (menu), event_panel_menu,
                       NULL, FALSE, 0);
  
  gtk_widget_ref (event_panel_menu[0].widget);
  gtk_object_set_data_full (GTK_OBJECT (control->window), "arkiv1",
                            event_panel_menu[0].widget,
                            (GtkDestroyNotify) gtk_widget_unref);
  
  gtk_widget_ref (event_panel_menu[1].widget);
  gtk_object_set_data_full (GTK_OBJECT (control->window), "redigera1",
                            event_panel_menu[1].widget,
                            (GtkDestroyNotify) gtk_widget_unref);
  
  gtk_widget_ref (event_panel_menu[2].widget);
  gtk_object_set_data_full (GTK_OBJECT (control->window), "toolsmenu",
                            event_panel_menu[2].widget,
                            (GtkDestroyNotify) gtk_widget_unref);
  
/****************************************************************/
  
  
/*** Create NOTEBOOK ***********/
  control->notebook = gtk_notebook_new ();
  gtk_widget_ref (control->notebook);
  gtk_object_set_data_full (GTK_OBJECT (control->window), "notebook", control->notebook,
                            (GtkDestroyNotify) gtk_widget_unref);
  gtk_signal_connect(GTK_OBJECT(control->notebook), "switch-page",
                            GTK_SIGNAL_FUNC(notebook_page_cb),NULL);
  gtk_widget_show (control->notebook);
  gtk_box_pack_start (GTK_BOX (vbox1), control->notebook, TRUE, TRUE, 0);
  
#if 0
/*** Scrolled WINDOW *************/
  
  scrolledwindow2 = gtk_scrolled_window_new (NULL, NULL);
  gtk_widget_ref (scrolledwindow2);
  gtk_object_set_data_full (GTK_OBJECT (control->window), "scrolledwindow2", scrolledwindow2,
                            (GtkDestroyNotify) gtk_widget_unref);
  gtk_widget_show (scrolledwindow2);
  gtk_container_add (GTK_CONTAINER (control->notebook), scrolledwindow2);
  
/*** Event LIST ****************/
  
  control->clist = gtk_clist_new (6);
  gtk_widget_ref (control->clist);
  gtk_object_set_data_full (GTK_OBJECT (control->window), "clist1", control->clist,
                            (GtkDestroyNotify) gtk_widget_unref);
  gtk_signal_connect(GTK_OBJECT(control->clist), "select_row",
                     GTK_SIGNAL_FUNC(eventlist_cb),NULL);
  
  gtk_widget_show (control->clist);
  gtk_container_add (GTK_CONTAINER (scrolledwindow2), control->clist);
  gtk_clist_set_column_width (GTK_CLIST (control->clist), 0, 40);
  gtk_clist_set_column_width (GTK_CLIST (control->clist), 1, 60);
  gtk_clist_set_column_width (GTK_CLIST (control->clist), 2, 60);
  gtk_clist_set_column_width (GTK_CLIST (control->clist), 3, 60);
  gtk_clist_set_column_width (GTK_CLIST (control->clist), 4, 60);
  gtk_clist_set_column_width (GTK_CLIST (control->clist), 5, 200);
  gtk_clist_column_titles_show (GTK_CLIST (control->clist));
  
  index = gtk_label_new (_("index"));
  gtk_widget_ref (index);
  gtk_object_set_data_full (GTK_OBJECT (control->window), "index", index,
                            (GtkDestroyNotify) gtk_widget_unref);
  gtk_widget_show (index);
  gtk_clist_set_column_widget (GTK_CLIST (control->clist), 0, index);
  
  stamp = gtk_label_new (_("Timestamp"));
  gtk_widget_ref (stamp);
  gtk_object_set_data_full (GTK_OBJECT (control->window), "stamp", stamp,
                            (GtkDestroyNotify) gtk_widget_unref);
  gtk_widget_show (stamp);
  gtk_clist_set_column_widget (GTK_CLIST (control->clist), 1, stamp);
  
  severity = gtk_label_new (_("Severity"));
  gtk_widget_ref (severity);
  gtk_object_set_data_full (GTK_OBJECT (control->window), "severity", severity,
                            (GtkDestroyNotify) gtk_widget_unref);
  gtk_widget_show (severity);
  gtk_clist_set_column_widget (GTK_CLIST (control->clist), 2, severity);
  
  source = gtk_label_new (_("Source"));
  gtk_widget_ref (source);
  gtk_object_set_data_full (GTK_OBJECT (control->window), "source", source,
                            (GtkDestroyNotify) gtk_widget_unref);
  gtk_widget_show (source);
  gtk_clist_set_column_widget (GTK_CLIST (control->clist), 3, source);
  
  summary = gtk_label_new (_("summary"));
  gtk_widget_ref (summary);
  gtk_object_set_data_full (GTK_OBJECT (control->window), "summary", summary,
                            (GtkDestroyNotify) gtk_widget_unref);
  gtk_widget_show (summary);
  gtk_clist_set_column_widget (GTK_CLIST (control->clist), 4, summary);
  
  arguments = gtk_label_new (_("arguments"));
  gtk_widget_ref (arguments);
  gtk_object_set_data_full (GTK_OBJECT (control->window), "arguments", arguments,
                            (GtkDestroyNotify) gtk_widget_unref);
  gtk_widget_show (arguments);
  gtk_clist_set_column_widget (GTK_CLIST (control->clist), 5, arguments);
#endif  
/****** statusbar ********/
  
  statusbar = gtk_statusbar_new ();
  gtk_widget_ref (statusbar);
  gtk_object_set_data_full (GTK_OBJECT (control->window), "statusbar", statusbar,
                            (GtkDestroyNotify) gtk_widget_unref);
  gtk_widget_show (statusbar);
  gtk_box_pack_start (GTK_BOX (vbox1), statusbar, FALSE, FALSE, 0);
  
  gtk_widget_set_usize (control->window, w_size, 200);

}   

