/*
 * GXSNMP - An snmp managment application
 * Copyright (C) 2000 Larry Liimatainen
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc.,  59 Temple Place - Suite 330, Cambridge, MA 02139, USA.
 *
 * gxevents.c -- GUI event display main
 *
 * The events & traps graphical interface
 */

#define USE_DATABASE 0

#ifdef HAVE_CONFIG_H
#include <config.h>
#endif


#include <gnome.h>
#include <stddef.h>
#include <stdio.h>
#include <errno.h>
#include <stdlib.h>
#include <sys/socket.h>
#include <sys/un.h>
#include <fcntl.h>
#include <unistd.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <sys/wait.h>
#include <netinet/in.h>
#include <libbonobo.h>

#include "dbapi.h"
#include "orbit-gxsnmp-db.h"
#include "gxsnmp/gxsnmp_dbapi.h"
#include "dae.h"
#include "gxevents_util.h" 

gchar gxevents_version[] = "$Id$"; /*usefull to 'strings' the binary*/

/*******************************
** PROTOTYPES                 **
*******************************/

       void fileacl_select();
       void fileacl_hide();
       void fileacl_load(GtkWidget *, GtkFileSelection *);
       void subfileacl_select();
       void subfileacl_hide();
       void subfileacl_load(GtkWidget *, GtkFileSelection *);
static void     update_event_panel           (gxsnmp_event    *event, GtkWidget * clist);
       void quit_cb();

/*******************************
** GLOBALS                    **
*******************************/

GxSNMPEventDisplay * control;

/*******************************
*******************************/


void quit_cb(){
  GList *gl;
  gxsnmp_evdpy *evdpy;

  gtk_widget_destroy(control->window);
  g_free(control);

  gl = control->evdpys;
  while(gl) {
    evdpy = gl->data;
    g_io_channel_close(evdpy->channel);
  }

  if(control->ddchannel)
    db_server_disconnect(control->ddchannel);

  exit(0);
}

void 
notebook_page_cb(GtkNotebook *notebook, 
                 GtkNotebookPage *page, gint page_num, gpointer user)
{
}

void
gxsnmp_gxsnmp_event_copy(gxsnmp_event *dst, gxsnmp_event *src)
{
  dst->eventid  = src->eventid;
  dst->category = src->category;
  dst->severity = src->severity;
  dst->stat     = src->stat;
  dst->timestamp= src->timestamp;
  dst->source   = src->source;
  dst->summary  = src->summary;
  dst->description = src->description;
  dst->type     = src->type;
  dst->data     = dst->data;
  return;
}

/* eventlist_cb()
*
* Fill in current selected event
* when user clicks on a event.
*/
void eventlist_cb(GtkWidget *eventlist, gint row, gint column, GdkEventButton *event){
gchar *field;
int a;
  GList     *item;
  gxsnmp_event *data;

  gtk_clist_get_text(GTK_CLIST(eventlist), row, 0, &field);
  a = atoi(field);						/*get eventid*/
  item = g_list_first (control->event_list);
  while (item)   /*search up using eventid in gxsnmp_events*/
    {
      data = (gxsnmp_event *)item->data;
      if (data)
	if ( (data->eventid) == a) /*ok, we've found the gxsnmp_event*/
	  {
            gxsnmp_gxsnmp_event_copy(&control->selected_event, data);
            break;
          }
      item = g_list_next (item);
    }
}

/***************************/

gpointer
gxsnmp_evdpy_lookup(GIOChannel *channel)
{
  gxsnmp_evdpy * evdpy;
  GList *gl;

  gl = control->evdpys;
  while(gl){
    evdpy = gl->data;
    if(evdpy->channel == channel){
      return evdpy->clist;
    }
    gl = gl->next;
  }
  return 0;
}

/*
 * List management functions.
 */

int add_event (GIOChannel *channel, gxsnmp_event *event)
{
  GList     *item;
  gxsnmp_event  *cp,*low=0,*data;
  int a ,b;
  GtkWidget * clist;

  /**** FIRST LOOKUP what widget 
        this channel belongs to 
  *****/

  if(!(clist = gxsnmp_evdpy_lookup(channel)))
    return 0;

  cp = (gxsnmp_event *)g_malloc (sizeof (gxsnmp_event));
  memcpy (cp, event, sizeof (gxsnmp_event));		/*make a copy of incoming event*/
  cp->eventid = ++(control->trapid);
  control->event_list = g_list_append (control->event_list, cp);
  b = g_list_length(control->event_list);
  while(b > control->env.gxevents_max_traps){		/*adjust list to maximum amount of list items*/
    gtk_clist_remove(GTK_CLIST (clist), 0);	/*remove oldest entry in list*/
    item = g_list_first (control->event_list);			/*update g_list*/
    a = 65535;						/*maximum possible amount of list items ever*/
    while (item)
      {
        data = (gxsnmp_event *)item->data;
        if (data)
          if (a > (data->eventid)){ 
            a = data->eventid;
            low = data;
          }
        item = g_list_next (item);
      }
    control->event_list = g_list_remove(control->event_list, low);
    g_free(low);
    b--;
  }
  update_event_panel(cp, clist);
  return 1;
}

int remove_event (int eventid)
{
  GList     *item;
  gxsnmp_event *data;

  item = g_list_first (control->event_list);
  while (item)
    {
      data = (gxsnmp_event *)item->data;
      if (data)
	if ( (data->eventid) == eventid)
	  {
	    control->event_list = g_list_remove (control->event_list, data);
	    g_free (data);
	    return 1;
	  }
      item = g_list_next (item);
    }
  return -1;
}


/*
 * misc functions
 */
static void 
update_event_panel (gxsnmp_event *event, GtkWidget * clist)
{
  char    t_buf[80];
  gchar    *list_item[8];
  int row;
  GdkColor *color;

  if (control)
    {
      if (GTK_WIDGET_VISIBLE (control->window))
	{
	  g_snprintf (t_buf, sizeof (t_buf), "%d", event->eventid);
	  list_item[0] = g_strdup (t_buf);
	  list_item[1] = g_strdup (ctime (&event->timestamp));
	  switch (event->severity)
	    {
	    case 1:
	      list_item[2] = g_strdup (_("Critical"));
              color = &control->red;
	      break;
	    case 2:
	      list_item[2] = g_strdup (_("Alarm"));
              color = &control->yellow;
	      break;
	    case 3:
	      list_item[2] = g_strdup (_("Warning"));
              color = &control->amber;
	      break;
	    case 4:
	      list_item[2] = g_strdup (_("Cleared"));
              color = &control->green;
	      break;
	    case 5:
	      list_item[2] = g_strdup (_("Informative"));
              color = &control->white;
	      break;

	    default:
	      list_item[2] = g_strdup ("Unknown");
              color = &control->grey;
	      break;
	    }
	  list_item[3] = g_strdup (event->source);
	  list_item[4] = g_strdup (event->summary);
	  list_item[5] = g_strdup (event->description);
	  row = gtk_clist_append (GTK_CLIST (clist), list_item);
          gtk_clist_set_foreground(GTK_CLIST (clist), row, &control->black );
          gtk_clist_set_background(GTK_CLIST (clist), row, color);
	}
    }
}

/*
 * Mainloop
 */

int main(int argc, char *argv[]){
  gchar line[2000];
  GMainLoop *loop;
  gchar *query;

  control = g_malloc(sizeof(GxSNMPEventDisplay));
  load_environment(&control->env);
  gnome_init("gxevents","0.1",argc,argv);
  event_display_init(control);
  gtk_widget_show_all (control->window);

  g_snprintf(line,400,"%s/conf/trap.conf", control->env.base);
  fprintf(stderr,"Loading trap.conf (%s)\n", line);
  control->env.traptab = load_trapconf(line);

/* load table-plugins and connect to database daemon */
  if(USE_DATABASE){ /* eventually this can be user controlled */
    CORBA_exception_init (&control->ev);

    bonobo_init (NULL, NULL);
    //control->orb = oaf_init (argc, argv);

    //query = g_strdup("repo_ids.has ('IDL:GxSNMP/DB:1.0')");
    //control->db_client = oaf_activate (query, NULL, 0, NULL, &control->ev);
    //g_free(query);
    if(control->db_client == CORBA_OBJECT_NIL) {
      g_print("DATABASE CONNECTION FAILED\n");
      exit(-1);
    }
  } else {
#if 0
    control->ddchannel = db_server_connect_config ("127.0.0.1", 4000);
    control->host_sqldb.channel = control->ddchannel;
    g_sqldb_table_load(control->host_sqldb);
#endif
  }

  control->aclfile[0] = 0;				/*set aclfile to state none loaded*/

/**** setup g_strsub ( string substitution ) list ********/
  control->strsub = g_strsub_list_add(control->strsub, 
             "$TIMESTAMP", &control->selected_event.timestamp);
  control->strsub = g_strsub_list_add(control->strsub, 
             "$SOURCE", &control->selected_event.source);
  control->strsub = g_strsub_list_add(control->strsub, 
             "$SUMMARY", &control->selected_event.summary);
  control->strsub = g_strsub_list_add(control->strsub, 
             "$DETAIL", &control->selected_event.description);
/*********************************/

  gtk_main ();

#if 0
  loop = g_main_new (TRUE);
  while (g_main_is_running (loop))
    {
      g_main_iteration(0);
      //if(gtk_events_pending()) gtk_main_iteration();
    }
  g_main_destroy (loop);
#endif
  exit(0);
}
