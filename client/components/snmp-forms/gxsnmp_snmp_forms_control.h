/* -*- Mode: C -*-
 * $Id$
 * GXSNMP - An snmp managment application
 * Copyright 1998 Gregory McLean
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Cambridge, MA 02139, USA.
 * 
 * The mib browser panel.
 */

#ifndef __GXSNMP_SNMP_FORMS_H__
#define __GXSNMP_SNMP_FORMS_H__
#include <gnome.h>
#include <bonobo.h>
#include <smi.h>

//BEGIN_GNOME_DECLS
/****************************************************************************
 * Standard widget macros 
 **/
#define GXSNMP_TYPE_SNMP_FORMS            (gxsnmp_snmp_forms_get_type ())
#define GXSNMP_SNMP_FORMS(obj)            (GTK_CHECK_CAST ((obj), GXSNMP_TYPE_BROWSER, GxSNMPSnmpForms))
#define GXSNMP_SNMP_FORMS_CLASS(klass)    (GTK_CHECK_CLASS_CAST (klass, GXSNMP_TYPE_BROWSER, GxSNMPSnmpFormsClass))
#define GXSNMP_IS_SNMP_FORMS(obj)         (GTK_CHECK_TYPE ((obj), GXSNMP_TYPE_SNMP_FORMS)
#define GXSNMP_IS_SNMP_FORMS_CLASS(klass) (GTK_CHECK_CLASS_TYPE ((klass), GXSNMP_TYPE_SNMP_FORMS)
/****************************************************************************
 * Control blocks for the widget class and for widget instances 
 **/
typedef struct _GxSNMPSnmpForms             GxSNMPSnmpForms;
typedef struct _GxSNMPSnmpFormsClass        GxSNMPSnmpFormsClass;

struct _GxSNMPNode
{
  SmiNode  *node;
  GtkEntry *entry;
  gchar    *cache;
};

struct _GxSNMPSnmpForms
{
  GtkVBox        vbox;           /* Parent Object */
  GtkVBox       *formbox;        /* Form Box */
  GSList        *nodes;
  gpointer       request;
  gchar		*hostname;       /* Hostname to Query */
  gchar		*rcom;           /* Read Community Name */
  gchar		*wcom;           /* Write Community Name */
  gchar		*form;           /* Form */
};

struct _GxSNMPSnmpFormsClass
{
  GtkVBoxClass    parent_class;
};
/****************************************************************************
 * Public Widget functions
 ***************************************************************************/
GtkType        gxsnmp_snmp_forms_get_type               (void);
GtkWidget      *gxsnmp_snmp_forms_new                   (void);

#endif

/* EOF */
