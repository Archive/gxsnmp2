/*
 * test.c
 * based on sample-control-container.c
 * 
 * Authors:
 *   Nat Friedman  (nat@helixcode.com)
 *   Michael Meeks (michael@helixcode.com)
 *
 * Copyright 1999, 2000 Helix Code, Inc.
 */
#include <config.h>
#include <gnome.h>
#include <bonobo.h>

static gchar    *hname = NULL;
static gchar    *rcom  = NULL;
static gchar    *wcom  = NULL;
static gchar    *form  = NULL;
poptContext      pctx;

struct poptOption options[] = {
  { "hostname", '\0', POPT_ARG_STRING, &hname, 0,
    N_("Hostname to be used for Rack Viewer control"), "HOSTNAME" },
  { "form", '\0', POPT_ARG_STRING, &form, 0,
    N_("Form to be used for Rack Viewer control"), "FORM" },
  { "rcom", '\0', POPT_ARG_STRING, &rcom, 0,
    N_("Read community name to be used for Rack Viewer control"), "RCOM" },
  { "wcom", '\0', POPT_ARG_STRING, &wcom, 0,
    N_("Write community name to be used for Rack Viewer control"), "WCOM" },
  { NULL, '\0', 0, NULL, 0 }
};

static void
app_destroy_cb (GtkWidget *app, BonoboUIContainer *uih)
{
	bonobo_object_unref (BONOBO_OBJECT (uih));

	gtk_main_quit ();
	/*g_warning ("Main level %d\n", gtk_main_level ());*/
}

static int
app_delete_cb (GtkWidget *widget, GdkEvent *event, gpointer dummy)
{
	gtk_widget_destroy (GTK_WIDGET (widget));
	return FALSE;
}

static guint
container_create (void)
{
	BonoboWindow      *app;
	GtkWidget         *control;
	GtkWindow	  *window;
	BonoboUIContainer *uih;
	GtkWidget	  *box;
	BonoboUIEngine    *engine;

	app = BONOBO_WINDOW (
              bonobo_window_new ("gxsnmp-test-control-container",
			         "GxSNMP Test control container"));

	window = GTK_WINDOW (app);

	uih = bonobo_ui_container_new ();
	engine = bonobo_window_get_ui_engine (app);
	bonobo_ui_container_set_engine (uih, engine);

	gtk_window_set_policy (window, TRUE, TRUE, FALSE);

	gtk_signal_connect (GTK_OBJECT (window), "delete_event",
		GTK_SIGNAL_FUNC (app_delete_cb), NULL);

	gtk_signal_connect (GTK_OBJECT (window), "destroy",
		GTK_SIGNAL_FUNC (app_destroy_cb), uih);

	box = gtk_vbox_new (FALSE, 0);
	
	bonobo_window_set_contents (BONOBO_WINDOW (app), box);
/*	bonobo_ui_handler_create_menubar (uih); */

	control = bonobo_widget_new_control (
		"OAFIID:gxsnmp-snmp_forms:e69e9dcd-47c2-4212-be86-74122eefbfa1",
		bonobo_object_corba_objref (BONOBO_OBJECT (uih)));

	if (control)
	  gtk_box_pack_start (GTK_BOX (box), control, TRUE, TRUE, 0);

	gtk_widget_show_all (GTK_WIDGET(app));

	if (hname)
		bonobo_widget_set_property ((BonoboWidget *)control,
					    "hostname", hname, NULL);
	if (rcom)
		bonobo_widget_set_property ((BonoboWidget *)control,
					    "rcom", rcom, NULL);
	if (wcom)
		bonobo_widget_set_property ((BonoboWidget *)control,
					    "wcom", wcom, NULL);
	if (form)
		bonobo_widget_set_property ((BonoboWidget *)control,
					    "form", form, NULL);
	return FALSE;
}

int
main (int argc, char **argv)
{
	CORBA_Environment ev;

	CORBA_exception_init (&ev);

	gnome_init_with_popt_table ("gxsnmp-test-control-container", "1.0", 
		argc, argv, options, 0, &pctx);
	poptFreeContext (pctx);

	if (bonobo_init (NULL, NULL) == FALSE)
		g_error ("Could not initialize Bonobo\n");

	/*
	 * We can't make any CORBA calls unless we're in the main
	 * loop.  So we delay creating the container here.
	 */
	gtk_idle_add ((GtkFunction) container_create, NULL);

	bonobo_main ();

	return 0;
}
