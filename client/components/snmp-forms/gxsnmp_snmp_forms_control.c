/* -*- Mode: C -*-
 * $Id$
 * GXSNMP - An snmp managment application
 * Copyright (C) 1998 Gregory McLean
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc.,  59 Temple Place - Suite 330, Cambridge, MA 02139, USA.
 *
 * The rack viewer panel.
 */
#ifdef HAVE_CONFIG_H
#include <config.h>
#endif

#include <gnome.h>

#include <bonobo.h>
#include <glade/glade.h>
#include <smi.h>
#include "g_snmp.h"

#include "gxsnmp_snmp_forms_control.h"
#include "gxsnmp_util.h"

#include "debug.h"

/****************************************************************************
 * Static data
 ***************************************************************************/

static const value_string snmp_error[] = {
  { SNMP_TOOBIG,                        "too big"},
  { SNMP_NOSUCHNAME,                    "no such name"},
  { SNMP_BADVALUE,                      "bad value"},
  { SNMP_READONLY,                      "read only"},
  { SNMP_GENERROR,                      "generic error"},
  { SNMP_NOACCESS,                      "no access"},
  { SNMP_WRONGTYPE,                     "wrong type"},
  { SNMP_WRONGLENGTH,                   "wrong length"},
  { SNMP_WRONGENCODING,                 "wrong encoding"},
  { SNMP_WRONGVALUE,                    "wrong value"},
  { SNMP_NOCREATION,                    "no creation"},
  { SNMP_INCONSISTENTVALUE,             "inconsisten value"},
  { SNMP_RESOURCEUNAVAILABLE,           "resource unavailable"},
  { SNMP_COMMITFAILED,                  "commit failed"},
  { SNMP_UNDOFAILED,                    "undo failed"},
  { SNMP_AUTHORIZATIONERROR,            "authorization error"},
  { SNMP_NOTWRITABLE,                   "not writable"},
  { SNMP_INCONSISTENTNAME,              "inconsistent name"},
  { 0, NULL}};

enum {
  PROP_HOSTNAME,
  PROP_RCOM,
  PROP_WCOM,
  PROP_FORM
};

static int object_count = 0;

/****************************************************************************
 * Forward declarations and callback functions  
 ***************************************************************************/
static void      viewer_class_init        (GxSNMPSnmpFormsClass      *klass);
static void      viewer_init              (GxSNMPSnmpForms           *control);
static void      get_button_cb            (GtkWidget                 *widget,
                                           gpointer                   data);
static void      put_button_cb            (GtkWidget                 *widget,
                                           gpointer                   data);
/****************************************************************************
 * Class registration
 ***************************************************************************/
/**
 * gxsnmp_snmp_forms_get_type:
 *
 * Registers class with GTK and returns handle ("type") of class. The handle
 * is cached in a static class variable for repeated calls of this function.
 *
 * Return value: unique type
 **/
GtkType
gxsnmp_snmp_forms_get_type ()
{
  static GtkType list_type = 0;
  D_FUNC_START;
  if (!list_type)
    {
      GtkTypeInfo list_info = 
      {
	"GxSNMPSnmpForms",
	sizeof (GxSNMPSnmpForms),
	sizeof (GxSNMPSnmpFormsClass),
	(GtkClassInitFunc) viewer_class_init,
	(GtkObjectInitFunc) viewer_init,
	/* reserved 1 */ NULL,
	/* reserved 2 */ NULL,
	(GtkClassInitFunc) NULL,
      };
      list_type =gtk_type_unique(gtk_vbox_get_type(), &list_info);
    }
  D_FUNC_END;
  return list_type;
}
/****************************************************************************
 * Static Helper functions
 ***************************************************************************/
/**
 * add_oid:
 * @objs: object structure
 * @oid: OID in SMI notation
 * @oidlen: length of OID
 * @iid: Instance in SMI notation
 * @iidlen: length of Instance
 * @type: type of object
 * @value: value of object
 *
 * setup host_snmp structure from object data.
 *
 **/
void
add_oid(GSList **objs, gint *oid, gint oidlen, gint *iid, gint iidlen,
        guchar type, gpointer value)
{
  gulong *mib;
  guint   miblen;
  D_FUNC_START;
  mib = g_malloc((oidlen + iidlen) * sizeof(gulong));
  for (miblen = 0; miblen < oidlen; miblen++)
    mib[miblen] = oid[miblen];
  for (miblen = 0; miblen < iidlen; miblen++)
    mib[oidlen + miblen] = iid[miblen];
  miblen = oidlen + iidlen;
  g_pdu_add_oid(objs, mib, miblen, type, value);
  D_FUNC_END;
}
/****************************************************************************
 * Class methods
 ***************************************************************************/
/**
 * viewer_class_init:
 * @klass: self 
 *
 * Constructor of this class.
 *
 **/
static void
viewer_class_init (GxSNMPSnmpFormsClass *klass)
{
  D_FUNC_START;
  D_FUNC_END;
}
/****************************************************************************
 * Object methods
 ***************************************************************************/
static void
register_oid(GtkWidget *widget, const gchar *snmp, GxSNMPSnmpForms *control)
{
  struct _GxSNMPNode *node;
  D_FUNC_START;
  node = g_malloc(sizeof(struct _GxSNMPNode));
  node->node  = smiGetNode(NULL, snmp);
  node->cache = g_strdup("");
  node->entry = GTK_ENTRY(widget);
  if (node->node)
    control->nodes = g_slist_append(control->nodes, node);
  else
    {
      g_free(node->cache);
      g_free(node);
    }
  D_FUNC_END;
}
static void
connect_snmp(GtkWidget *widget, GxSNMPSnmpForms *control)
{
  const gchar *snmp;
  D_FUNC_START;
  if (GTK_IS_ENTRY(widget))
    {
      snmp = gtk_entry_get_text(GTK_ENTRY(widget));
      /* debug */
      g_warning("adding %s to SNMP OIDs", snmp);
      register_oid(widget, snmp, control);
      gtk_entry_set_text(GTK_ENTRY(widget), "");
    }
  if (GTK_IS_CONTAINER(widget))
    {
      gtk_container_foreach(GTK_CONTAINER(widget), connect_snmp, control);
    }
  D_FUNC_END;
}
/**
 * form_init:
 * @control: control
 *
 * Constructs form from XML (libglade) source.
 *
 **/
static void
form_init (GxSNMPSnmpForms *control)
{
  GladeXML           *xml;
  GtkWidget          *form;
  GList              *children;
  struct _GxSNMPNode *node;
  D_FUNC_START;
  control->request = NULL;
  while (control->nodes)
    {
       node = (struct _GxSNMPNode *) control->nodes->data;
       control->nodes = control->nodes->next;
       g_free(node->cache);
       g_free(node);
     }
  control->nodes = NULL;
  children = gtk_container_children(GTK_CONTAINER(control->formbox));
  if (children)
    {
      form = children->data;
      gtk_container_remove(GTK_CONTAINER(control->formbox), form);
    }
  xml = glade_xml_new(control->form, "form", NULL);
  glade_xml_signal_autoconnect(xml);
  form = glade_xml_get_widget(xml, "form");
  connect_snmp(form, control);
  gtk_widget_show_all(form);
  gtk_box_pack_start (GTK_BOX (control->formbox), form, TRUE, TRUE, 0);
}
/**
 * viewer_init:
 * @control: self 
 *
 * Constructor of this object.
 *
 * Constructs GUI objects.
 *
 **/
static void
viewer_init (GxSNMPSnmpForms *control)
{
  GtkWidget       *button_box;
  GtkWidget       *get;
  GtkWidget       *put;
  D_FUNC_START;
  control->nodes = NULL;
  control->form = g_strdup("/usr/share/gxsnmp/forms/default.glade");
  control->formbox = gtk_vbox_new (FALSE, 1);
  gtk_box_pack_start (GTK_BOX (control), control->formbox, TRUE, TRUE, 0);
  gtk_widget_show(control->formbox);
  form_init(control);
  button_box = gtk_hbutton_box_new ();
  gtk_container_border_width (GTK_CONTAINER (button_box), 2);
  gtk_button_box_set_layout (GTK_BUTTON_BOX (button_box), GTK_BUTTONBOX_SPREAD);
  gtk_button_box_set_spacing (GTK_BUTTON_BOX (button_box), 2);
  gtk_box_pack_start (GTK_BOX (control), button_box, TRUE, TRUE, 0);
  get = gtk_button_new_with_label (_("Get"));
  gtk_container_add (GTK_CONTAINER (button_box), get);
  put = gtk_button_new_with_label (_("Put"));
  gtk_container_add (GTK_CONTAINER (button_box), put);
  gtk_widget_show_all(button_box);
  /* Connect Signals */
  gtk_signal_connect (GTK_OBJECT (get), "clicked",
    GTK_SIGNAL_FUNC (get_button_cb), control);
  gtk_signal_connect (GTK_OBJECT (put), "clicked",
    GTK_SIGNAL_FUNC (put_button_cb), control);
  control->request = NULL;
  control->hostname = g_strdup("localhost");
  control->rcom = g_strdup("public");
  control->wcom = g_strdup("public");
  D_FUNC_END;
}
/****************************************************************************
 * Object signals
 ***************************************************************************/
/****************************************************************************
 * Signals of other objects
 ***************************************************************************/
static struct _GxSNMPNode *
get_node(struct _SNMP_OBJECT *obj, GxSNMPSnmpForms *control)
{
  GSList *nodes;
  struct _GxSNMPNode * node;
  nodes = control->nodes;
  while(nodes)
    {
      node = nodes->data;
      if (node->node->oidlen + 1 == obj->id_len)
	{
	  if (!memcmp(node->node->oid, obj->id, node->node->oidlen * sizeof(guint)))
	    return node;
	}
      nodes = nodes->next;
    }
  return NULL;
}
/**
 * update_mib_value (host_snmp *host, void *magic, SNMP_PDU *spdu, GSList *objs) * @host: host_snmp structure
 * @magic: data attached to callback
 * @spdu: SNMP PDU carrying variables
 * @objs: variables in SNMP object form
 *
 **/
gboolean
update_mib_value (host_snmp *host, void *magic, SNMP_PDU *spdu, GSList *objs)
{
  char    buf[1024], *ptr;
  struct _SNMP_OBJECT *obj;
  struct _GxSNMPNode *node;
  GxSNMPSnmpForms *control;
  D_FUNC_START;
  g_return_if_fail (magic != NULL);
  control = (GxSNMPSnmpForms *) magic;
  control->request = NULL;
  while (objs)
    {
      obj = objs->data;
      if (obj) g_snmp_printf (buf, sizeof(buf), obj);
      node = get_node(obj, control);
      if (node)
	{
	  if (spdu->request.error_status)
	    {
	      ptr = match_strval(spdu->request.error_status, snmp_error);
	      if (ptr) strcpy(buf, ptr);
	      else snprintf(buf, 1024, "unknown error %d", 
		spdu->request.error_status);
	    }
	  g_free(node->cache);
	  node->cache = g_strdup(buf);
	  gtk_entry_set_text (node->entry, buf);
	}
      objs = objs->next;
    }
  D_FUNC_END;
  return TRUE;
}
/**
 * update_mib_timeout (host_snmp *host, void *magic)
 * @host: host_snmp structure
 * @magic: data attached to callback
 *
 **/
void
update_mib_timeout (host_snmp *host, void *magic)
{
  GxSNMPSnmpForms *control;
  D_FUNC_START;
  g_return_if_fail (magic != NULL);
  control = (GxSNMPSnmpForms *) magic;
  control->request = NULL;
#if 0
  gtk_widget_set_style(control->mib_value, control->error);
  gtk_entry_set_text (GTK_ENTRY (control->mib_value), "timeout");
#endif
  D_FUNC_END;
}
/**
 * setup_host:
 * @widget: self
 * @host: pointer to host_snmp structure
 *
 * setup host_snmp structure from object data.
 *
 **/
void
setup_host (GxSNMPSnmpForms *control, host_snmp *host)
{
  D_FUNC_START;
  host->domain        = AF_INET;
  host->rcomm         = control->rcom;
  host->wcomm         = control->wcom;
  host->retries       = 5;
  host->name          = control->hostname;
  host->status        = 0;
  host->port          = 161;
  host->timeout       = 10;
  host->version       = SNMP_V1;
  host->done_callback = update_mib_value;
  host->time_callback = update_mib_timeout;
  host->magic         = control;
  D_FUNC_END;
}
/**
 * get_button_cb:
 * @widget: self
 * @data: pointer to control object
 *
 * SIGNAL Method to announce press of GET button
 *
 **/
static void
get_button_cb (GtkWidget* widget, gpointer data)
{
  gint            instance[1];
  struct _GxSNMPNode *node;
  GxSNMPSnmpForms *control;
  GSList         *objs;
  GSList         *nodes;
  host_snmp       host;
  D_FUNC_START;
  g_return_if_fail (data != NULL);
  control = (GxSNMPSnmpForms *) data;
  if (control->request)
    g_remove_request(control->request);
  nodes = control->nodes;
  setup_host(control,&host);
  objs = NULL;
  instance[0] = 0;
  while (nodes)
    {
      node = nodes->data;
      g_warning("Adding OID for node %s", node->node->name);
      add_oid(&objs, node->node->oid, node->node->oidlen, instance, 1, 
	SNMP_NULL, NULL);
      nodes = nodes->next;
    }
  control->request = g_async_get(&host, objs);
  D_FUNC_END;
}
/**
 * put_button_cb:
 * @widget: self
 * @data: pointer to control object
 *
 * SIGNAL Method to announce press of PUT button
 *
 **/
static void
put_button_cb (GtkWidget* widget, gpointer data)
{
  gint            instance[1];
  struct _GxSNMPNode *node;
  GxSNMPSnmpForms *control;
  GSList         *objs;
  GSList         *nodes;
  host_snmp       host;
  char           *value;
  guint32         val;
  SmiType        *type;
  D_FUNC_START;
  g_return_if_fail (data != NULL);
  control = (GxSNMPSnmpForms *) data;
  if (control->request)
    g_remove_request(control->request);
  nodes = control->nodes;
  setup_host(control,&host);
  objs = NULL;
  instance[0] = 0;
  while (nodes)
    {
      node = nodes->data;
      type = smiGetNodeType(node->node);
      value = gtk_entry_get_text(GTK_ENTRY(node->entry));
      if (strcmp(value, node->cache))
	{
          switch(type->basetype)
            {
              case SMI_BASETYPE_UNKNOWN:
              case SMI_BASETYPE_INTEGER64:
              case SMI_BASETYPE_FLOAT32:
              case SMI_BASETYPE_FLOAT64:
              case SMI_BASETYPE_FLOAT128:
              case SMI_BASETYPE_BITS:
              case SMI_BASETYPE_OBJECTIDENTIFIER:  /* XXX */
                /* Not supported in the moment */
                break;
              case SMI_BASETYPE_INTEGER32:
              case SMI_BASETYPE_UNSIGNED32:
              case SMI_BASETYPE_UNSIGNED64:
              case SMI_BASETYPE_ENUM:  /* XXX */
                val = atoi(value);
                add_oid(&objs, node->node->oid, node->node->oidlen, instance, 
		  1, SNMP_INTEGER, &val);
                break;
              case SMI_BASETYPE_OCTETSTRING:
                add_oid(&objs, node->node->oid, node->node->oidlen, instance, 
		  1, SNMP_OCTETSTR, value);
                break;
	    }
        }
      nodes = nodes->next;
    }
  if (objs)
    control->request = g_async_set(&host, objs);
  D_FUNC_END;
}
/****************************************************************************
 * Public API
 ***************************************************************************/
/**
 * gxsnmp_snmp_forms_new:
 *
 * Creates new Rack Viewer object
 *
 * Return value: Rack Viewer object
 **/
GtkWidget *
gxsnmp_snmp_forms_new ()
{
  GxSNMPSnmpForms  *control;
  D_FUNC_START;
  control = gtk_type_new (gxsnmp_snmp_forms_get_type ());
  D_FUNC_END;
  return GTK_WIDGET (control);
}
/****************************************************************************
 * Bonobo functions
 ***************************************************************************/
static void
set_prop (BonoboPropertyBag *bag, const BonoboArg *arg, guint arg_id,
	  CORBA_Environment *ev, gpointer user_data)
{
  GxSNMPSnmpForms *control;
  D_FUNC_START;
  g_return_if_fail (user_data != NULL);
  control = (GxSNMPSnmpForms *) user_data;
  switch (arg_id)
    {
      case PROP_HOSTNAME:
	g_free(control->hostname);
	control->hostname = g_strdup(BONOBO_ARG_GET_STRING (arg));
	break;
      case PROP_RCOM:
	g_free(control->rcom);
	control->rcom = g_strdup(BONOBO_ARG_GET_STRING (arg));
	break;
      case PROP_WCOM:
	g_free(control->wcom);
	control->wcom = g_strdup(BONOBO_ARG_GET_STRING (arg));
	break;
      case PROP_FORM:
	g_free(control->form);
	control->form = g_strdup(BONOBO_ARG_GET_STRING (arg));
	form_init(control);
	break;
    }
  D_FUNC_END;
}
static void
get_prop (BonoboPropertyBag *bag, BonoboArg *arg, guint arg_id, 
	  CORBA_Environment *ev, gpointer user_data)
{
  GxSNMPSnmpForms *control;
  D_FUNC_START;
  g_return_if_fail (user_data != NULL);
  control = (GxSNMPSnmpForms *) user_data;
  switch (arg_id)
    {
      case PROP_HOSTNAME:
	BONOBO_ARG_SET_STRING (arg, control->hostname);
	break;
      case PROP_RCOM:
	BONOBO_ARG_SET_STRING (arg, control->rcom);
	break;
      case PROP_WCOM:
	BONOBO_ARG_SET_STRING (arg, control->wcom);
	break;
      case PROP_FORM:
	BONOBO_ARG_SET_STRING (arg, control->form);
	break;
    }
  D_FUNC_END;
}
static void
gxsnmp_snmp_forms_destroyed(GtkObject *obj)
{
  object_count--;
  if (object_count <= 0) {
    gtk_main_quit ();
  }
}
static BonoboObject *
gxsnmp_snmp_forms_factory (BonoboGenericFactory *Factory, void *closure)
{
  BonoboPropertyBag *pb;
  BonoboControl     *control;
  GtkWidget         *viewer;

  D_FUNC_START;
  viewer = gxsnmp_snmp_forms_new ();
  gtk_widget_show (viewer);

  /* Create the control. */
  control = bonobo_control_new (viewer);
  pb = bonobo_property_bag_new (get_prop, set_prop, viewer);
  bonobo_control_set_properties (control, pb, NULL);
  bonobo_property_bag_add (pb, "hostname", PROP_HOSTNAME, BONOBO_ARG_STRING,
    NULL, "Hostname", 0);
  bonobo_property_bag_add (pb, "rcom", PROP_RCOM, BONOBO_ARG_STRING,
    NULL, "Read Community Name", 0);
  bonobo_property_bag_add (pb, "wcom", PROP_WCOM, BONOBO_ARG_STRING,
    NULL, "Write Community Name", 0);
  bonobo_property_bag_add (pb, "form", PROP_FORM, BONOBO_ARG_STRING,
    NULL, "Form", 0);
  bonobo_control_set_automerge (control, TRUE);
  object_count++;
  gtk_signal_connect (GTK_OBJECT (viewer), "destroy",
     gxsnmp_snmp_forms_destroyed, NULL);  
  D_FUNC_END;
  return BONOBO_OBJECT (control);
}

void
gxsnmp_snmp_forms_factory_init (void)
{
  static BonoboGenericFactory *snmp_forms_control_factory = NULL;

  D_FUNC_START;
  if (snmp_forms_control_factory != NULL)
    return;
#if USING_OAF
  snmp_forms_control_factory = bonobo_generic_factory_new (
    "OAFIID:gxsnmp-snmp_forms-factory:7f565f76-7bb1-4da2-9cc7-d88f6337e92c",
    gxsnmp_snmp_forms_factory, NULL);
#else
  snmp_forms_control_factory = bonobo_generic_factory_new (
    "control-factory:gxsnmp-snmp_forms", gxsnmp_snmp_forms_factory, NULL);
#endif

  if (snmp_forms_control_factory == NULL)
    g_error ("I could not register a Rack Viewer factory.");
  D_FUNC_END;
}

/* EOF */
