/* -*- Mode: C -*-
 * $Id$
 *
 * Document shell for gxsnmp.
 */
#include <config.h>
#include <gnome.h>
#include <bonobo.h>
#include <glade/glade.h>

#define __IN_MAIN_C__
#include "debug.h"

/****************************************************************************
 *
 */
/* Session management functions */
static void                session_die        (GnomeClient        *client,
					       gpointer           data);
static gint                save_session       (GnomeClient        *client,
					       gint               phase,
					       GnomeSaveStyle     save_style,
					       gint               is_shutdown,
					       GnomeInteractStyle style,
					       gint               is_fast,
					       gpointer           client_data);
/** popt goo */
void                       debug_file_cb      (poptContext             con,
					       enum poptCallbackReason reason,
					       const struct poptOption *opt,
					       const char              *arg,
					       const void              *data);

/**
 * popt goo
 **/
/* Debuging infrastructure */
int             debug_level  = 0;
GHashTable      *debug_files = NULL;
static gchar    *dummy       = NULL;
poptContext     pctx;
struct poptOption options[] = {
  { NULL, '\0', POPT_ARG_CALLBACK, debug_file_cb, "NOTNULL", NULL, NULL },
  { "debug-level", '\0', POPT_ARG_INT, &debug_level, 0,
    N_("Specify the level of debugging messages that will be output."),
    "DEBUG_LEVEL" },
  { "debug-filename", '\0', POPT_ARG_STRING, &dummy, 0,
    N_("Specify the filename for which debugging messages will be output (can "
       "be specified more than once)."),
    N_("DEBUG_FILENAME") },
  { NULL, '\0', 0, NULL, 0 }
};
void
debug_file_cb (poptContext con, enum poptCallbackReason reason,
               const struct poptOption *opt, const char *arg,
               const void *data)
{
  if (!strcmp(opt->longName, "debug-filename"))
    {
      if (!debug_files)
        debug_files = g_hash_table_new(g_str_hash, g_str_equal);
      g_hash_table_insert(debug_files, arg, data);
    }
}

int
main (int argc, char *argv[])
{
  GnomeClient       *client;              /* Session management client */
  
  bindtextdomain (PACKAGE, GNOMELOCALEDIR);
  textdomain (PACKAGE);
  
  gnome_init_with_popt_table (PACKAGE, VERSION, argc, argv,
				    options, 0, &pctx);
  poptFreeContext (pctx);

  bonobo_init (&argc, argv);
  
  //  glade_gnome_init ();
  
  /*** Do any application initilization */

  /** Keep this bit as the last thing we do before dropping into the
      application loop proper. */
  /*** Contact the session manager and get ths session client initilized */
  client = gnome_master_client ();
  gtk_signal_connect (GTK_OBJECT (client), "save_yourself",
		      GTK_SIGNAL_FUNC (save_session), argv[0]);
  gtk_signal_connect (GTK_OBJECT (client), "die",
                      GTK_SIGNAL_FUNC (session_die), NULL);

  gtk_main (); /* Start interacting with the user */
  return EXIT_SUCCESS;
  
}


/** Session management **/
/***
 * save_session:
 **/
static gint
save_session (GnomeClient *client, gint phase, GnomeSaveStyle save_style,
              gint is_shutdown, GnomeInteractStyle interact_style,
              gint is_fast, gpointer client_data)
{
  gchar **argv;
  guint argc;

  argv = g_malloc0(sizeof (gchar *)*4);
  argc = 1;

  argv[0] = client_data;
  gnome_client_set_clone_command (client, argc, argv);
  gnome_client_set_restart_command (client, argc, argv);
  /* Save any addtional states here */
  return TRUE;
}
/**
 * session_die
 **/
static void
session_die (GnomeClient *client, gpointer client_data)
{
  gtk_main_quit ();
}

/* EOF */


