/* -*- Mode: C; tab-width: 8; indent-tabs-mode: nil; c-basic-offset: 4 -*-
 * $Id$
 * gxsnmp_db_server.h -- Private server includes, only needed for the
 *                       database server implementation.
 *
 */
#ifndef __GXSNMP_DB_SERVER_H__
#define __GXSNMP_DB_SERVER_H__
#ifdef HAVE_CONFIG_H
#include <config.h>
#endif
#include <glib.h>

/*
 * The server's instance
 */
typedef struct _gxsnmp_db_server {

  G_sql_connection   *db_handle;    /* Handle for the database */
  
  GList      *clients;              /* Connected clients */
  GMainLoop  *loop;                 /* The main loop */
  
} gxsnmp_db_server;

/*
 * The server's idea of a client.
 */
typedef struct _gxsnmp_db_client {
  gchar       *incoming_buf;         /* Buffer for incomming packets */
  GIOChannel  *io_channel;           /* Client's stream */
  gint        flags;                 /* Flags relavant to this client */
} gxsnmp_db_client;

#endif
