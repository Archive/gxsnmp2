/* -*- Mode: C; tab-width: 8; indent-tabs-mode: nil; c-basic-offset: 4 -*-
 * $Id$
 * tcp_service.h   -- Server tcp handling code.
 *
 */
#ifndef __TCP_SERVICE_H__
#define __TCP_SERVICE_H__
#ifdef HAVE_CONFIG_H
#include <config.h>
#endif

#include <glib.h>

/*
 * This is a totally event driven service and reacts to the relevant changes
 * in the sockets. New connection, activity, closed connection.
 * Writing to clients is driven by the clients themselves, they request from
 * the server, the server finds the client's instance, performs the request
 * and queues up a send with a callback attached to the G_IO_WRITE state,
 * and the response is fed to the client as fast as it can accept it all
 * driven off the event chain.
 */
/***
 * Public functions
 **/

gboolean        tcp_socket_init                (gchar     *address,
						gint      port);
gboolean        tcp_socket_shutdown            (void);

#endif
