#!/bin/sh
# Run this to generate all the initial makefiles, etc.

srcdir=`dirname $0`
test -z "$srcdir" && srcdir=.

PKG_NAME="GXSNMP"

(test -f $srcdir/configure.in \
  && test -d $srcdir/client) || {
    echo -n "**Error**: Directory "\`$srcdir\'" does not look like the"
    echo " top-level gxsnmp directory"
    exit 1
}

. $srcdir/autogen2.sh
